#!/bin/php
#SE Factory: Assignment 2 - Exercise 2
#Author: Adnan Balhawan
#Date: 23rd June, 2017

<?php

$entries = file('/var/log/apache2/access.log');

foreach($entries as $entry_num => $entry){

	//extract ip address
	$initialExplode = explode("-", $entry);
	$ipaddress = $initialExplode[0];
	
	//extract Http request message
	$secondExplode = explode("\"", $entry);
	$HttpReq = $secondExplode[1];

	//extract response code
	$resExplode = explode (" ", $secondExplode[2]);
	$resCode = $resExplode[1];

	//extracting date and time
	$TimeStampExplode = explode("/\[(.+)\]/", $secondExplode[0]);
	$cleanTimeStamp = substr($TimeStampExplode[0], 2, -7);

	//get substring and extract date and time
	$DateTime = explode(":",$cleanTimeStamp);
	$hour = $DateTime[1];
	$minutes = $DateTime[2];
	$seconds = $DateTime[3];

	$Date = explode('/', $DateTime[0]);
	$Day = $Date[0];
	$Month = $Date[1];
	$Year = $Date[2];

	//converting extracted date to unified unix timestamp
	$dateFromatFinal = "$Day - $Month - $Year";
	$UnixTimeStamp = strtotime($dateFromatFinal);

	//convert day and month to alpahnumeric value
	$LongFormatDay = date('l', $UnixTimeStamp);
	$LongFormatMonth = date('F', $UnixTimeStamp);

	//output standard format for all entries in access.log
	echo "$ipaddress -- $LongFormatDay, $LongFormatMonth, $Year : $hour-$minutes-$seconds -- $HttpReq -- $resCode\n";

	}

?>
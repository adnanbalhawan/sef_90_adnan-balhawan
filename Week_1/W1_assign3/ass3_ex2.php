#!/bin/php
<?php
#SE Factory: Assignment 3 - Exercise 2 (Code Golf)
#Author: Adnan Balhawan
#Date: 23rd June, 2017

$src = false;

while($src == false)
{
	echo "src:(num hats)(path) \n";

	$start =readline('');

	if(strpos($start, '/') !== FALSE)
	{
		$words = file($start);
		$src = true;
	}

	else if (is_numeric($start))
	{
		echo "start entering words: \n";

		for ($index=0; $index < $start; $index++) 
		{
	        $line = readline();
	        readline_add_history($line);
	        $words[$index] = $line;
		}
		$src = true;
	}

	else 
	{
		echo "\nERROR, try again\n\n";
	}
}

$fl = str_split('qwertyuiop');
$sl = str_split('asdfghjkl');
$tl = str_split('zxcvbnm');

foreach($words as $word)
{
	$sw = str_split(rtrim($word));

	$result1 = array_intersect($sw, $fl);
	$result2 = array_intersect($sw, $sl);
	$result3 = array_intersect($sw, $tl);

	if($sw == $result1 or $sw == $result2 or $sw == $result3)
	{
		$nc[] = count($sw);
		$nw[] = implode($sw);		
	}

}

$l = array_map('strlen', $nw);
$ml = max($l);
$i = array_search($ml, $l);
echo ".\n".$nw[$i]."\n";


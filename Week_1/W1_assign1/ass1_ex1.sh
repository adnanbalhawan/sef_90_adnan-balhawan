#!/bin/bash
# Assignment 1 - excersize 1
# June 21, 2016

#sets the directory to /var/log
cd /var/log/

#list content of directory and subdirectory that is a .log file, in additon to file size in KBs
ls -l -a -R --block-size=K | egrep '\.log' | awk '{print $9",",$5/1}' > /home/adnan/Desktop/log_dump.csv
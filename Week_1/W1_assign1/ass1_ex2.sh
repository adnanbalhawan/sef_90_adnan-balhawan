#!/bin/bash
# Assignment 1 - excercise 2
# June 21, 2016

#check memory usage and prints alarm if above or equal to 80%
free -m | grep 'Mem' | awk '{if (($3*100/$2)>=80) print "ALARM: Virtual Memory is at",$3*100/$2,"%"; else if (($3*100/$2)<80) y=1; }'

#checks mounted drive usage and prints alarm if above or equal to 80%
df -H | grep -vE '^Filesystem|tmpfs|cdrom|sr0' | awk '{if ($5>=800) print "ALARM: Disk",$1,"is at",$5; else if ($5<800) x=1; }'

if (x=1 && y=1)
	then
		echo Everything is ok
	fi
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    // protected $table = 'posts';
    
    function Comment ()
    {
        $this->hasMany('comments');
    }
}

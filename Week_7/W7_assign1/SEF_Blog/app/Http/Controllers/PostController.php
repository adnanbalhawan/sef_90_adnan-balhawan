<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class PostController extends Controller
{
    function AllPosts ()
    {
        $posts = DB::table('posts')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.id', 'title', 'body', 'user_id',
                        'users.name', 'posts.updated_at', 'posts.created_at')
                    ->orderBy('updated_at')
                    ->paginate(5);

        return view('posts', ['posts' => $posts]);
    }

    function show ($post_id)
    {
        $post = DB::table('posts')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->where('posts.id', '=', $post_id)
                    ->get();

        $comments = DB::table('comments')
                    ->join('users', 'comments.user_id', '=', 'users.id')
                    ->join('posts', 'comments.post_id', '=', 'posts.id')
                    ->where('comments.post_id', '=', $post_id)
                    ->orderBy('comments.created_at')
                    ->get();
        
        return view('post', ['post' => $post, 'comments' => $comments]);
    }

    function NewPost ()
    {
        return view('new_post');
    }

    public function addPost (Request $request)
    {
        DB::table('posts')->insert(
            ['user_id' => $request->user_id, 
            'title' => $request->title, 
            'body' => $request->body,
            'edited' => 0,
            'created_at' => date('Y-m-d H:i:s')]
        );        
        
        return view('status.success');
    }

    public function edit ($post_id)
    {
        $post = DB::table('posts')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->where('posts.id', '=', $post_id)
                    ->get();   
        
        return view('editPost', ['post' => $post]);
    }
    
    public function update (Request $request)
    {
        DB::table('posts')
            ->where('id', '=', $request->id)
            ->update(
            ['title' => $request->title,
            'body' => $request->body,
            'updated_at' => date('Y-m-d H:i:s')]); 
        
        return view('status.update');
    }

    public function delete ($post_id)
    {
        DB::table('posts')->delete($post_id);        
        
        return view('status.delete');
    }
}

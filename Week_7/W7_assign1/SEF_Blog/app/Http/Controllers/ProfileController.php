<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class ProfileController extends Controller
{
    public function show ($id)
    {
        $posts = DB::table('posts')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.id', 'title', 'body', 'user_id',
                        'users.name', 'posts.updated_at', 'posts.created_at')
                    ->where('users.id', '=', $id)
                    ->orderBy('updated_at')
                    ->paginate(5);

        return view('profile.profile', ['posts' => $posts]);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Post::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'user_id' =>$faker->numberBetween($min = 1, $max = 400),
        'title' => $faker->realText(40),
        'body' => $faker->realText(2000),
        'edited' =>$faker->numberBetween($min = 0, $max = 1),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'user_id' =>$faker->numberBetween($min = 1, $max = 400),
        'post_id' =>$faker->numberBetween($min = 1, $max = 800),
        'content' => $faker->realText(75),
        'edited' =>$faker->numberBetween($min = 0, $max = 1),
    ];
});

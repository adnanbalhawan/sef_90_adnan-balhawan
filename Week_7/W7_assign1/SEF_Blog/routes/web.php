<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('/posts');
});

Auth::routes();

Route::get('/home', function() {
    return redirect('/posts');
});

Route::get('/posts', 'PostController@AllPosts');

Route::get('/posts/{post_id}', 'PostController@show');

Auth::routes();

Route::get('/new_post', 'PostController@NewPost');

Route::post('/posts/add', 'PostController@addPost');

Route::post('/posts/update/', 'PostController@update');

Route::get('/posts/edit/{post_id}', 'PostController@edit');

Route::get('/posts/delete/{post_id}', 'PostController@delete');

Route::get('/user/{id}', 'ProfileController@show');

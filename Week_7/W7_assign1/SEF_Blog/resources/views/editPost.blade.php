@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if (Auth::guest())
                <h1>Please log in to add new posts!</h1>
            @else
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Edit your blog post,</h3>
                        <form method='POST' action="/posts/update" >
                            <div class='form-gorup'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{ $post[0]->id }}">
                                <label for="title">Title</label>
                                <input type="text" id ='title' class='form-control' name='title' value="{{ $post[0]->title }}" required>
                            </div>
                            <br>
                            <div class='form-gorup'>
                                <label for="body">Body</label>
                                <textarea name="body" id='body' class='form-control' rows='5' required>{{ $post[0]->body }}</textarea>
                                <br>
                            </div>
                            <div class='form-gorup'>
                                <button class="btn btn-primary" type='submit'>Update Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection

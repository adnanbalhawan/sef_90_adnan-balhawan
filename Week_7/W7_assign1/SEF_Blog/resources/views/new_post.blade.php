@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if (Auth::guest())
                <h1>Please log in to add new posts!</h1>
            @else
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Create a new blog post,</h3>
                        <p>Make it something amazing!!</p>
                        <form method='POST' action="/posts/add" >
                            <div class='form-gorup'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <label for="title">Title</label>
                                <input type="text" id ='title' class='form-control' name='title' required>
                            </div>
                            <br>
                            <div class='form-gorup'>
                                <label for="body">Body</label>
                                <textarea name="body" id='body' class='form-control' rows='5' required></textarea>
                                <br>
                            </div>
                            <div class='form-gorup'>
                                <button class="btn btn-primary" type='submit'>Create Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection

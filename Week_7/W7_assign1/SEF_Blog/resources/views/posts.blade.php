@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @foreach ($posts as $key => $post)
                <div class="panel panel-success">
                    <div class='panel-heading'>
                        <a href="/posts/{{ $post->id }}">
                            <h3> {{ $post->title }} </h3>
                        </a>

                        <p> 
                            <a href="{{ '/user/'.$post->user_id }}">
                                {{ $post->name }} 
                            </a>
                            
                            at 

                            @if ($post->updated_at != null 
                                and $post->created_at < $post->updated_at)
                                {{ $post->updated_at }} 
                            @else
                                {{ $post->created_at }}
                            @endif
                        </p>
                    </div>
                    <div class="panel-body">

                        @if (Auth::guest())
                            
                        @elseif ($post->user_id == Auth::user()->id)

                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" 
                                    type="button" id="dropdownMenu1" data-toggle="dropdown" 
                                    aria-haspopup="true" aria-expanded="true">
                                    <span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu" aria-labelledby="dLabel" role="menu">
                                    <li>
                                        <a href="{{ '/posts/edit/'.$post->id }}">
                                            Edit
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ '/posts/delete/'.$post->id }}">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        @endif

                        <p> {{ str_limit($post->body, 250) }} </p>
                    </div>
                </div>   
            @endforeach
            
            <div class="panel panel-success">
                {{ $posts->links() }}
            </div>     

        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                @foreach ($post as $info)
                    <div class="panel-heading"><h1> {{ $info->title }} </h1></div>
                    <div class="panel-body">
                    @if (Auth::guest())
                        
                    @elseif ($info->user_id == Auth::user()->id)

                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" 
                                type="button" id="dropdownMenu1" data-toggle="dropdown" 
                                aria-haspopup="true" aria-expanded="true">
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" aria-labelledby="dLabel" role="menu">
                                <li>
                                    <a href="{{ '/posts/edit/'.$info->id }}">
                                        Edit
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="{{ '/posts/delete/'.$info->id }}">
                                        Delete
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @endif

                        <p> 
                            <a href="{{ '/user/'.$info->user_id }}">
                                {{ $info->name }} 
                            </a>
                            
                            at 

                            @if ($info->updated_at != null 
                                and $info->created_at < $info->updated_at)
                                {{ $info->updated_at }} 
                            @else
                                {{ $info->created_at }}
                            @endif
                        </p>
                        <p> {{ $info->body }} </p>
                    </div> 
                @endforeach

                <div class="panel-footer">comments</div>
                @foreach ($comments as $comment)
                        <ul class="list-group">
                            <li class="list-group-item">
                                <p> {{ $comment->name }} </p>
                                <p>at {{ $comment->updated_at}} </p>
                                <p> {{ $comment->content }} </p>
                            </li>
                        </ul>        
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
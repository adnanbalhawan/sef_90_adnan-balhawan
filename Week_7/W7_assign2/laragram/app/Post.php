<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'img', 'caption',
    ];

    public function likes()
    {
        return $this->belongsToMany('App\User', 'likes');
    }

    public function comments()
    {
        return $this->belongsToMany('App\Comment', 'comments');
    }

}

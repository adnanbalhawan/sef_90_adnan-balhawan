<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;

class LikeController extends Controller
{
    public function index(Request $request)
    {
        Like::insert(
            ['user_id' => $request->user_id,
            'post_id' => $request->post_id]
        );
        
        return redirect()->action('PostController@view',
             ['id' => $request->post_id]);
    }

    public function unlike(Request $request)
    {
        $like = Like::find($request->id);
        $like->delete();

        return redirect()->action('PostController@view',
             ['id' => $request->post_id]);
    }
}

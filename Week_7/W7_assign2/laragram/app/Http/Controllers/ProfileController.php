<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function index($username)
    {
        $posts = User::join('posts', 'users.id', 'posts.user_id')
                    ->select('posts.id', 'posts.img',
                        'users.user_name')
                    ->where('users.user_name', '=', $username)
                    ->orderBy('posts.created_at')
                    ->get();

        $userInfo = User::select('*')
                    ->where('users.user_name', '=', $username)
                    ->get();

        if(count($userInfo))
        {
            return view('pages.profile',
                ['userInfo' => $userInfo[0],
                'posts' => $posts]);
        }
        else
        {
            return view('status.notfound');
        }
    }

    public function edit(Request $request)
    {
        $profile = User::find($request->user_id);

        return view('forms.profile', ['profile' => $profile]);
    }

    public function update(Request $request)
    {
        $profile = User::find($request->user_id);

        if($request->bio != null)
        {
            $profile->bio = $request->bio;    
        }
        if($request->gender != null)
        {
            $profile->gender = $request->gender;    
        }
        if($request->website != null)
        {
            $profile->website = $request->website;    
        }
        if($request->dob != null)
        {
            $profile->dob = $request->dob;    
        }

        $profile->save();

        return $this->index($profile->user_name); 
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function add(Request $request)
    {
        Comment::insert(
            ['user_id' => $request->user_id,
            'post_id' => $request->post_id,
            'content' => $request->comment]
        );

        return redirect()->action('PostController@view',
             ['id' => $request->post_id]);
    }

    public function update(Request $request)
    {
        $comment = Comment::find($request->id);
        $comment->content = $request->content;    
        $profile->save();

        return redirect()->action('PostController@view',
             ['id' => $request->post_id]);
    }

    public function delete(Request $request)
    {
        $comment = Comment::find($request->id);
        $comment->delete();

        return redirect()->action('PostController@view',
             ['id' => $request->post_id]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Post;
use App\Comment;
use App\Like;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::join('users', 'posts.user_id', 'users.id')
                    ->select('posts.id', 'img', 'caption', 'posts.user_id',
                        'posts.updated_at', 'posts.created_at',
                        'users.user_name', 'users.picture')
                    ->orderBy('posts.created_at', "DESC")
                    ->get();
        
        $comments = Comment::join('users', 'comments.user_id', 'users.id')
                    ->select('comments.post_id', 'comments.user_id')
                    ->orderby('comments.created_at')
                    ->get();

        if(Auth::guest())
        {
        return view('pages.home',
                ['posts' => $posts,
                'comments' => $comments]);
        }
        else
        {
            $likes = Like::select('post_id', 'id')
                ->where('likes.user_id', '=', Auth::user()->id)
                ->get();
            
            return view('pages.home',
                ['posts' => $posts,
                'comments' => $comments,
                'likes' => $likes]);
        }
    }

    public function view($id)
    {
        $posts = Post::join('users', 'posts.user_id', 'users.id')
                    ->select('posts.id', 'img', 'caption', 'posts.user_id',
                        'posts.updated_at', 'posts.created_at',
                        'users.user_name', 'users.picture')
                    ->where('posts.id', '=', $id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
        
        $comments = Comment::join('users', 'comments.user_id', 'users.id')
                    ->select('comments.post_id', 'comments.content', 
                        'comments.user_id', 'users.user_name')
                    ->where('comments.post_id', '=', $id)
                    ->orderby('comments.created_at', 'desc')
                    ->get();

        if(Auth::guest())
        {
        return view('pages.post',
                ['posts' => $posts,
                'comments' => $comments]);
        }
        else
        {
            $likes = Like::select('post_id', 'id')
                ->where('likes.user_id', '=', Auth::user()->id,
                    'and', 'post.id', '=', $id)
                ->get();
            
            return view('pages.post',
                ['posts' => $posts,
                'comments' => $comments,
                'likes' => $likes]);
        }

    }

    public function new()
    {
        return view('forms.post');
    }

    public function add(Request $request)
    {
        $post = new Post;
        $post->user_id = $request->user_id;
        $post->img = $request->file('image')->getClientOriginalName();
        $post->caption = $request->caption;

        $request->file('image')->store('./img/uploads');
        $post->save();

        return redirect('/home');
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('forms.edit', ['post' => $post]);
    }

    public function update(Request $request)
    {
        $post = Post::find($request->id);
        $post->caption = $request->caption;  
        $post->save();

        return $this->view($request->id);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();

        return $this->index();
    }
}

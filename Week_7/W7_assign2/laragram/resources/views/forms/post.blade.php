@extends('layouts.app')

@section('content')


<script>
    function readURL(input) 
    {
        if (input.files && input.files[0]) 
        {
            var reader = new FileReader();

            document.getElementById('view').setAttribute("width", "100%");
            reader.onload = function (e) {
                $('#view')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        
            @if (Auth::guest())
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Please log in or register to add new posts!</h3>
                        <input type="button" class="btn btn-default" value="Login" 
                            onclick="location.href = '{{ route('login') }}'">                 
                        <input type="button" class="btn btn-default" value="Register" 
                            onclick="location.href = '{{ route('register') }}'"> 
                    </div>
                </div>
            @else
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>upload a new picture</h3>
                        <p>Make it something amazing!!</p>

                        <img id="view"/>

                        <form method="POST" action="/post/add" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="img-responsive">Image</label>
                                <input type='file' onchange="readURL(this);" name='image'>
                                <p class="help-block">only jpg, and jpeg images are allowed</p>
                            </div>
                            <div class='form-gorup'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <label for="img">caption</label>
                                <input type="text" id ='caption' class='form-control' name='caption' required>
                            </div>
                            <br>
                            <div class='form-gorup'>
                                <button class="btn btn-primary" type='submit'>Create Post</button>
                            </div>

                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
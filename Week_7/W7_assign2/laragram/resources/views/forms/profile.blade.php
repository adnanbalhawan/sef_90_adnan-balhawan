@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        
            @if (Auth::guest())
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Please log in or register to add new posts!</h3>
                        <input type="button" class="btn btn-default" value="Login" 
                            onclick="location.href = '{{ route('login') }}'">                 
                        <input type="button" class="btn btn-default" value="Register" 
                            onclick="location.href = '{{ route('register') }}'"> 
                    </div>
                </div>
            @else
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>edit your profile</h3>
                        <p>Make it something amazing!!</p>
                        <form method='POST' action="/profile/update" files='true'>
                            <div class="form-group">
                                <label for="dob">date of birth</label>
                                <input type="date" id ='dob' class='form-control' name='dob'>
                            </div>
                            <div class="form-group">
                                <select class="custom-select">
                                    <option selected>Gender</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Other</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="website">website</label>
                                <input type="text" id ='website' class='form-control' name='website' value='{{ $profile->website }}'>
                            </div>
                            <div class='form-gorup'>
                                <label for="bio">bio</label>
                                <textarea name="bio" id='bio' class='form-control' rows='5'>{{ $profile->bio }}</textarea>
                            </div>
                            <br>
                            <div class='form-gorup'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <button class="btn btn-primary" type='submit'>Update Profile</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>

@endsection
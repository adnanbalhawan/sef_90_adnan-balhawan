@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        
            @if (Auth::guest())
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Please log in or register to add new posts!</h3>
                        <input type="button" class="btn btn-default" value="Login" 
                            onclick="location.href = '{{ route('login') }}'">                 
                        <input type="button" class="btn btn-default" value="Register" 
                            onclick="location.href = '{{ route('register') }}'"> 
                    </div>
                </div>
            @else
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Update post caption</h3>
                        <form method="POST" action="/post/update">
                            <div class='form-gorup'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="id" value="{{ $post->id }}">
                                <label for="caption">caption</label>
                                <input type="text" id ='caption' class='form-control' name='caption' value="{{ $post->caption }}" required>
                            </div>
                            <br>
                            <div class='form-gorup'>
                                <button class="btn btn-primary" type='submit'>Update Post</button>
                            </div>

                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
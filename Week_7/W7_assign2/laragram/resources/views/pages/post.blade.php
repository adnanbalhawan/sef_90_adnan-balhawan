@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @foreach ($posts as $key => $post)
                <div class="panel panel-default">
                    
                    <div class='panel-heading'>

                        <a href="/profile/{{ $post->user_name }}">
                            @if($post->picture == 'placeholder.png')
                                <img class="img-circle" src="../../img/{{ $post->picture }}" alt="" style="width:8%">
                            @else
                                <img class="img-circle" src="../../storage/img/users/{{ $post->picture }}" alt="" style='width: 8%'>
                            @endif
                            <b> {{ $post->user_name }} </b>
                        </a>
                    
                    </div>

                    <div class="panel-body panel-custom">    
                        <img class="img-responsive" src="../../storage/img/uploads/{{ $post->img }}" alt="" style="width:100%">
                    </div>

                    <div class="panel-body">
                        
                        @if (Auth::guest())
                            <form action='/post/new' method='POST'>
                                <div class='form-group'>
                                    <button class="btn btn-default" type='submit'>
                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </form>
                        @else
                            <?php $liked = false ?>
                            @foreach($likes as $key => $like)
                                @if($like->post_id == $post->id)
                                    <form action='/unlike' method='POST'>
                                        <div class='form-group'>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                                            <input type="hidden" name="id" value="{{ $like->id }}">
                                            <button class="btn btn-default" type='submit'>
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                    <?php $liked = true; ?>
                                @else
                                @endif
                            @endforeach
                            @if($liked == false)
                                <form action='/like' method='POST'>
                                    <div class='form-group'>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                        <button class="btn btn-default" type='submit'>
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </form>
                            @endif
                        @endif       
                        <p>
                            <a href="/profile/{{ $post->user_name }}">
                                <b>{{ $post->user_name }}</b> 
                            </a>
                            {{ $post->caption }}
                        </p>
                    </div>

                    <div class='panel-body'>
                        @foreach($comments as $key => $comment)
                            @if($comment->post_id == $post->id)
                                <p>
                                    <a href="/profile/{{ $comment->user_name }}">
                                        <b>{{ $comment->user_name }}</b> 
                                    </a>
                                    {{ $comment->content }}
                                </p>
                            @endif
                        @endforeach
                    </div>

                    @if (Auth::guest())
    
                    @else
                        <div class='panel-body'>
                            <form action='/comment/add' method='POST'>
                                <div class='form-group'>
                                    <input type="text" name='comment' id='comment' class='form-control comment'>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                    <button class="btn btn-default" type='submit'>
                                        comment
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    @endif

                    @if (Auth::guest())
                        <div class="dropdown">
                            <div class='text-right'>
                                <button class="btn btn-default dropdown-toggle" 
                                    type="button" id="dropdownMenu1" data-toggle="dropdown" 
                                    aria-haspopup="true" aria-expanded="true">
                                    <span class="fa fa-ellipsis-h"></span>
                                </button>

                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel" role="menu">
                                    <li>
                                        <a href="{{ '/post/view/'.$post->id }}">
                                            Go to Post
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @elseif ($post->user_id == Auth::user()->id)

                        <div class="dropdown">
                            <div class='text-right'>
                                <button class="btn btn-default dropdown-toggle" 
                                    type="button" id="dropdownMenu1" data-toggle="dropdown" 
                                    aria-haspopup="true" aria-expanded="true">
                                    <span class="fa fa-ellipsis-h"></span>
                                </button>

                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel" role="menu">
                                    <li>
                                        <a href="{{ '/post/edit/'.$post->id }}">
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ '/post/delete/'.$post->id }}">
                                            Delete
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ '/post/view/'.$post->id }}">
                                            Go to Post
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>    
                    @endif

                </div>   
            @endforeach

        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

</div>
<div class='container-fluid'>
    <div class='row'>
        <div class='col-sm-4'>
            @if($userInfo->picture == 'placeholder.png')
                <img class="img-circle" src="../img/{{ $userInfo->picture }}" alt="" style="width:150px">
            @else
                <img class="img-circle" src="../storage/img/users/{{ $userInfo->picture }}" alt="" style='width: 150px'>
            @endif
        </div>
        <div class='col-sm-8'>
            <h2><b> {{ $userInfo->user_name }} </b></h2>

            <h4><b>{{ $userInfo->name }} </b> {{ $userInfo->bio }}  </h4>
            
            @if(Auth::user()->id == $userInfo->id)
                <form action="/profile/edit" method='POST'>
                    <div class='form-group'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <button class="btn btn-default" type='submit'>
                            Edit Profile
                        </button>
                    </div>
                </form>
            @endif
        </div>
    </div>

    <br><br>     
    @foreach ($posts as $key => $post)

            <div class="custom-grid">
                <div class="img-container">
                    <a href="/post/view/{{ $post->id }}">
                        <img src="../storage/img/uploads/{{ $post->img }}" class="img" style="width:100%">
                        <div class="middle">
                            <div class="img-text"><i class='fa fa-heart-o fa-2x'></i></div>
                        </div>
                    </a>
                </div>
            </div>
        
    @endforeach

</div>

@endsection

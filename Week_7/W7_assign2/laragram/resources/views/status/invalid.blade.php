@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3>Invalid input,</h3>
            <h1>:(</h1>
            <h3>try again <a href="/post/new">here</a></h3>
        </div>
    </div>
</div>

@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3>User not found</h3>
            <h1>:(</h1>
        </div>
    </div>
</div>

@endsection

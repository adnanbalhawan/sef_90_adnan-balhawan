<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
    return redirect('/home');
});

Auth::routes();

/**
 * Post Routes
 */

Route::get('/home', 'PostController@index');

Route::get('/post/view/{id}', 'PostController@view'); 

Route::get('/post/new', 'PostController@new'); 

Route::post('/post/add', 'PostController@add');

Route::get('/post/edit/{id}', 'PostController@edit');

Route::post('/post/update', 'PostController@update');

Route::get('/post/delete/{id}', 'PostController@delete');

/**
 * Profile routes
 */
Route::get('/profile/{username}', 'ProfileController@index');  

Route::post('/profile/edit', 'ProfileController@edit');  

Route::post('/profile/update', 'ProfileController@update');  

/**
 * Comment routes
 */
Route::post('/comment/add', 'CommentController@add');

Route::post('/comment/edit', 'CommentController@edit');

Route::post('/comment/update', 'CommentController@update');

Route::post('/comment/delete', 'CommentController@delete');

/**
 * Like routes
 */
Route::post('/like', 'LikeController@index');

Route::post('/unlike', 'LikeController@unlike');

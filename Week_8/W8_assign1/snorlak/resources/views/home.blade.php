@extends('layouts.app')

@section('content')


  <!-- Begin page content -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-7 col-sm-2 col-md-2 sidebar sidebar-left sidebar-animate sidebar-md-show">
        <ul class="nav navbar-stacked">
          <div class="navbar-header">

              <!-- Collapsed Hamburger -->
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                  <span class="sr-only">Toggle Navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>

          </div>

          <div class="collapse navbar-collapse" id="app-navbar-collapse">
              <!-- Left Side Of Navbar -->
              <ul class="nav navbar-nav">
                  &nbsp;
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="nav navbar-nav">
                  <!-- Authentication Links -->
                  @if (Auth::guest())
                      <li><a href="{{ route('login') }}">Login</a></li>
                      <li><a href="{{ route('register') }}">Register</a></li>
                  @else
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                              <h4><b>{{ Auth::user()->name }}</b> <span class="caret"></span> </h4>
                          </a><br>

                          <ul class="dropdown-menu" role="menu">
                              <li>
                                  <a href="{{ route('logout') }}"
                                      onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                      Logout
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                  </form>
                              </li>
                          </ul>
                      </li>
                  @endif
              </ul>
          </div>
          <h4><b>Channels</b></h4>
          <div class="panel-body" id='channels'></div>
          <br>
          <h4><b>Users</b></h4>
          <div class="fixed-sidebar" id='users'></div>

          <br><p>
            <b>Tips:</b><br>
            <code>:close:</code> to exit the page
            <code>:clear:</code> to clear messages from your screen
          </p>
        </ul>
      </div>
      <div class="main col-md-10">

        <div class="content-header" id="content-header"></div>
          <div class='fixed-panel' id='log'></div>
          <div class="panel-body">
            <input type="hidden" id="address" value="{{ $_SERVER['SERVER_ADDR'] }}">
            <input type="hidden" id='username' value="{{ Auth::user()->name }}">
            <input type="hidden" id='user_id' value="{{ Auth::user()->id }}">
            <input type="hidden" id="channel" value="main">
            <div class="form-group">
              <input id="msg" type="text" class='form-control' placeholder='Message' required>
            </div>
          </div>
        </div>
    </div>
  </div>

@endsection

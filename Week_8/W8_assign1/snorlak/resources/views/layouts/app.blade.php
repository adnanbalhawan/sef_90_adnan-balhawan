<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
<link id="favicon" rel="shortcut icon" href="../img/snorlak_logo.png" sizes="16x16 32x32 48x48" type="image/png" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

</head>
<body>
    {{-- <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">

            </div>
        </nav>
    </div> --}}

    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/snorlak.js') }}"></script>
</body>
</html>


@foreach ($messages as $key => $msg)
  <div class="panel-body">
    <div class="col-sm-1">
      <img class="img-rounded" src="../img/placeholder.png" alt="Generic placeholder image" width='50px'>
    </div>
    <div class="col-sm-10">
      <h5 class="mt-0"><b>{{ $msg->name }}</b> at {{ $msg->created_at }}</h5>
      {{ $msg->message }}
    </div>
  </div>
@endforeach

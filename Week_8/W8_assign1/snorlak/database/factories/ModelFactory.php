<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Channel::class, function (Faker\Generator $faker) {

    return [
        'name' => "SE Factory",
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Message::class, function (Faker\Generator $faker) {

    return [
        'member_id' => $faker->numberBetween($min = 1, $max = 20),
        'message' => $faker->realText(100),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Member::class, function (Faker\Generator $faker) {

    return [
      'user_id' => $faker->numberBetween($min = 1, $max = 10),
      'channel_id' => $faker->numberBetween($min = 1, $max = 5),
    ];
});
// factory(App\User::class, 2)->create()->each(function($u) {
//   $u->issues()->save(factory(App\Issues::class)->make());
// });

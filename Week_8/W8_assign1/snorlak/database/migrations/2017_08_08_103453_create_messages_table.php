<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('messages', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('member_id')
              ->unsigned()->nullable();
          $table->foreign('member_id')
              ->references('id')->on('members')
              ->onDelete('Set Null');

          $table->text('message');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('messages', function (Blueprint $table) {
        $table->dropForeign('messages_member_id_foreign');
      });

      Schema::dropIfExists('messages');
    }
}

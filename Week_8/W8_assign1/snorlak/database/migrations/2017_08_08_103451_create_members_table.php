<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('members', function (Blueprint $table) {
        $table->increments('id');

        $table->integer('user_id')
            ->unsigned()->nullable();
        $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('Set Null');

        $table->integer('channel_id')
            ->unsigned()->nullable();
        $table->foreign('channel_id')
            ->references('id')->on('channels')
            ->onDelete('Set Null');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('members', function (Blueprint $table) {
        $table->dropForeign('members_user_id_foreign');
        $table->dropForeign('members_channel_id_foreign');
      });

      Schema::dropIfExists('members');
    }
}

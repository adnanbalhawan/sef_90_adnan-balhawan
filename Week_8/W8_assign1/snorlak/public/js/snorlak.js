var SNORLAK =
{
  socket: '',
  host: '',
  channel: '',
  serverIP: document.getElementById('address').value,
  user: document.getElementById('username').value,
  userId: document.getElementById('user_id').value,

  init: function()
  {
    this.host = 'ws://'+this.serverIP+':3422/snorlak';

    this.getChannelHistory();
    this.getChannels();
    this.getUsers();

    try {
        this.socket = this.createSocket(this.host);
        this.socket.onopen = function(msg) {
            if(this.readyState == 1)
            {
              SNORLAK.log(SNORLAK.user + " joined");
            }
        };
        this.socket.onmessage = function(msg) {
            SNORLAK.log(msg.data);
        };
        this.socket.onclose = function(msg) {
            SNORLAK.log('<h5>' + SNORLAK.user + " is Offline</h5>");
        };
    }
    catch (ex) {
        this.log(ex);
    }
    document.getElementById("msg").focus();
  },

  createSocket: function()
  {
    if ('WebSocket' in window)
        return new WebSocket(this.host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(this.host);

    throw new Error("No web socket support in browser!");
  },

  send: function()
  {
    var msg = document.getElementById('msg').value;
    var timestamp = this.getDate();

    if(msg == ':clear:')
    {
      document.getElementById('log').innerHTML = '';
      this.log('<h3> Screen has been cleared! </h3>');
    }
    else if(msg == ':close:')
    {
      this.quit();
    }
    else if(msg != '' || msg !=' ')
    {
      formatmsg =
          "<div class='panel-body'>" +
          "  <div class='col-sm-1'>" +
          "    <img class='img-rounded' src='../img/placeholder.png' alt='placeholder' width='50px'>" +
          "  </div>" +
          "  <div class='col-sm-10'>" +
          "    <h5 class='mt-0'><b>" + this.user + "</b> at " + timestamp + "</h5>"+
          "    " + msg +
          "  </div>"+
          "</div>";

      SNORLAK.store(msg);

      try
      {
          this.socket.send(formatmsg);
          document.getElementById('msg').value='';
      }
      catch (ex)
      {
          SNORLAK.log(ex);
      }
    }
  },

  quit: function()
  {
    this.socket.close();
    this.log("Goodbye!<br> going back home :D ");
    this.socket = null;

    window.setTimeout(function()
    {
        window.location.href = "/";
    }, 1000);
  },

  log: function(msg)
  {
    var log = document.getElementById("log");
    log.innerHTML += msg;
    log.scrollTop = log.scrollHeight;
  },

  logSidebarChannels: function(channels)
  {
    var channelsLog = document.getElementById("channels");
    channels = JSON.parse(channels);
    for(var i = 0; i < channels.length; i++)
    {
      channelsLog.innerHTML += "<button href='/home'"+
        "class='btn btn-primary btn-custom'>"+channels[i].name+'</button><br>';
    }

    document.getElementById('content-header').innerHTML
      = '<h4><b>' + channels[0].name + '</b></h4>';
  },

  logSidebarUsers: function(users)
  {
    var usersLog = document.getElementById("users");
    users = JSON.parse(users);
    for(var i = 0; i < users.length; i++)
    {
      usersLog.innerHTML += "<button href='/home'"+
        "class='btn btn-primary btn-custom'>"+users[i].name+'</button><br>';
    }
  },

  /**
   * HTTP requests to Controllers
   */
  store: function(msg)
  {
    store = new XMLHttpRequest;
    store.open('get', '/message/store/1/'+msg);
    store.onreadystatechange = function ()
    {
      if (store.status == 500)
      {
        SNORLAK.log(store.responseText);
      }
      else if (store.status == 200 && store.readyState == 4)
      {
        SNORLAK.log(store.responseText);
      }
    };

    store.send();
  },

  getChannelHistory: function()
  {
    channelHistory = new XMLHttpRequest;
    channelHistory.open('get', '/channel/history/');
    channelHistory.onreadystatechange = function ()
    {
      if(channelHistory.readyState == 4 && channelHistory.status == 200)
      {
        SNORLAK.log(channelHistory.responseText);
      }
      else if(channelHistory.status == 500)
      {
        alert('server error, please refresh the page');
      }
    };

    channelHistory.send();
  },

  getChannels: function()
  {
    availableChannels = new XMLHttpRequest;
    availableChannels.open('get', '/user/channels/');
    availableChannels.onreadystatechange = function ()
    {
      if(availableChannels.readyState == 4 && availableChannels.status == 200)
      {
        SNORLAK.logSidebarChannels(availableChannels.response);
      }
      else if(availableChannels.status == 500)
      {
        alert('server error, please refresh the page');
      }
    };

    availableChannels.send();
  },

  getUsers: function()
  {
    available = new XMLHttpRequest;
    available.open('get', '/users/channel/');
    available.onreadystatechange = function ()
    {
      if(available.readyState == 4 && available.status == 200)
      {
        SNORLAK.logSidebarUsers(available.response);
      }
      else if(available.status == 500)
      {
        alert('server error, please refresh the page');
      }
    };

    available.send();
  },

  /**
   * Date formatting function
   */
  getDate: function(date)
  {
    if(!date)
      var d = new Date();
    else
      var d = new Date(date);

    var day = d.getDay();
    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds();

    if (min < 10) {
        min = "0" + min;
    }
    var ampm = hr < 12 ? "AM" : "PM";
    var date = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();

    var format = hr + ":" + min;
    //date + "/" + month + "/" + year + " at " +
    return format;
  },

};

SNORLAK.init();

document.getElementById('msg').addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13)
    {
      SNORLAK.send();
    }
});

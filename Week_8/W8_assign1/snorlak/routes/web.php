<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/channel/history/', 'ChannelController@open');

Route::get('/user/channels/', 'ChannelController@availableChannels');

Route::get('/users/channel/', 'ChannelController@availableUsers');

Route::get('/message/store/{channel}/{msg}', 'ChatController@store');

Route::get('/channel/join/{channel}', 'ChannelController@join');

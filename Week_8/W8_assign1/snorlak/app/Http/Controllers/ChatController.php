<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Message;
use App\Member;

class ChatController extends Controller
{
    function store($channel, $msg)
    {
      $member = Member::select('members.id')
                    ->where('members.user_id', '=', Auth::user()->id)
                    ->where('members.channel_id', '=', $channel)
                    ->get();

      $message = new Message;
      $message->member_id = $member[0]->id;
      $message->message = $msg;
      $message->save();
    }
}

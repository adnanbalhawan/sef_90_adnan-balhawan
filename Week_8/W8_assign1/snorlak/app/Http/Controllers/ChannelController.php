<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Message;
use App\Channel;
use App\Member;

class ChannelController extends Controller
{
    public function open()
    {
      $messages = Message::join('members', 'messages.member_id', 'members.id')
                      ->join('users', 'members.id', 'users.id')
                      ->select('messages.message', 'users.name'
                        , 'messages.created_at')
                      ->where('members.channel_id', '=', '1')
                      ->orderBy('messages.created_at', 'ASC')
                      ->get();

      return view('layouts.messages', ['messages' => $messages]);
    }

    public function availableChannels()
    {
      $channels = Channel::join('members', 'channels.id', 'members.channel_id')
                      ->join('users', 'members.user_id', 'users.id')
                      ->select('channels.name', 'channels.id')
                      ->where('users.id', '=', Auth::user()->id)
                      ->get();

      return $channels;
    }

    public function availableUsers()
    {
      $users = Channel::join('members', 'channels.id', 'members.channel_id')
                      ->join('users', 'members.user_id', 'users.id')
                      ->select('users.name', 'users.id')
                      ->where('members.channel_id', '=', 1)
                      ->get();

      return $users;
    }

    public function join($channel)
    {
      $member = new Member;
      $member->user_id = Auth::user()->id;
      $member->channel_id = $channel;
      $member->save();

      return redirect('/home');
    }
}

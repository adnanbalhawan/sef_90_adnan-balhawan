<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
  function messages()
  {
    return $this->hasMany('App\Message');
  }
}

var canvas = document.getElementById("thecanvas");
var ctx = canvas.getContext("2d");

var a;
var b;
var c;

var fps;
var now;
var then;
var interval;
var delta;
var disc;

reset();

function reset() 
{
  a = ['a', 200, 8, 7, 6, 5, 4, 3, 2, 1];
  b = ['b', 350];
  c = ['c', 500];
  fps = 0.5;
  then = Date.now();
  interval = 1000/fps;
  disc = 8;
  
  drawShapes(a,b,c);
}



function start() 
{
  draw();
}


function hanoi (disc, src, aux, dst) 
{
  if (disc > 0)
  {

    temp = src.pop();

    dst.push(temp);

    // ctx.fillStyle = '#000000';
    // ctx.fillText(src,50,50);
    // ctx.fillText(aux,50,100);
    // ctx.fillText(dst,50,150); 

    coords = getCoords(src, dst);
    animatePathDrawing(ctx, src[1]-(8*temp), 150+(16*temp), coords ,50 ,dst[1]-(8*temp) , 150+(16*temp), 1000, src, dst, aux, temp);

  } 
}


function sleep (ms)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}


function draw() {

	requestAnimationFrame(draw);
	now = Date.now();
	delta = now - then;
 
	if (delta > interval && disc > 0)  {
    then = now - (delta % interval);
    
    disc -= 1;
    hanoi(disc-1, a, c, b);
    hanoi(disc, a, b, c);
    hanoi(disc-1, c, a, b);
    // ctx.fillStyle = '#000000'
    // ctx.fillText(a,50,50);
     
    // // await sleep(500);
    // ctx.fillText(b,50,80);
  }    
}

function getCoords (src, dst)
{
  if(src[0] == 'a' && dst[0] == 'b')
  {
    return(275);
  }
  else if(src[0] == 'b' && dst[0] == 'c')
  {
    return(425);
  }
  else if(src[0] == 'b' && dst[0] == 'a')
  {
    return(275);
  }
  else if(src[0] == 'c' && dst[0] == 'b')
  {
    return(425);
  }
  else if(src[0] == 'a' && dst[0] == 'c')
  {
    return(350);
  }
  else if(src[0] == 'c' && dst[0] == 'a')
  {
    return(350);
  }
}

function drawShapes (a, b, c)
{

  ctx.fillStyle = '#DDDEEE';
  ctx.fillRect(0,0,canvas.width, canvas.height);

  ctx.fillStyle = "#FF0000";
  ctx.fillRect(100,300,500,8);

  ctx.fillStyle = "#000000";
  ctx.fillRect(198,150,4,150);
  ctx.fillRect(348,150,4,150);
  ctx.fillRect(498,150,4,150);

  var i; 

  for(i = 2; i < a.length; i++)
  {
    temp = a[i];
    drawCylinder(ctx, a[1]-(8*temp),150+(16*temp),16*temp,20);
  }

  i = 2;

  for(i = 2; i < b.length; i++)
  {
    temp = b[i];
    drawCylinder(ctx, b[1]-(8*temp),150+(16*temp),16*temp,20);
  }

  i = 2;
  for(i = 2; i < c.length; i++)
  {
    temp = c[i];
    drawCylinder(ctx, c[1]-(8*temp),150+(16*temp),16*temp,20);
  }
  
}
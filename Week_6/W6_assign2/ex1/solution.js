function invertPass(n) {
    var all = 'abcdefghijklmnopqrstuvwxyz',
        out = '',
        offset;

    while (n > 0) {
        offset = n % 17;
        out = all.charAt(offset - 1) + out;
        n = (n - offset) / 17;
    }
    return out;
}


function createPass(password) {
    var total = 0;
    var charlist = "abcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < password.length; i++) {
            var countone = password.charAt(i);                           
            var counttwo = (charlist.indexOf(countone));                    
            counttwo++;                                                 
            total *= 17;                                                
            total += counttwo;                                              
        }

    return total;
}

// var orig = 'gdclhpdhbied',
//     num = createPass(orig);

// console.log(invertPass(num) === orig);

console.log(invertPass(248410397744610));



function checkPass( password ) 
{ 
    var total = 0 ;
    var charlist = "abcdefghijklmnopqrstuvwxyz" ;

    for ( var i = 0; i < password.length; i++ ) 
    {
        var countone = password.charAt ( i ) ; // 0 returns h / 1 r g
        var counttwo = ( charlist.indexOf( countone )) ; // h returns 7 / g is 6
        counttwo++; // 7 becmes 8 / 7 b 8
        total *= 17; //0*17 = 0 // 8*17=136
        total += counttwo; //0+8=8 / 136+8=144
    }

    if ( total == 248410397744610 )    
    { 
        setTimeout (
            "location.replace('index.php?password="+password
            + "' ) ; " , 0 ) ;
    } 

    else 
    { 
        alert (
            "Sorry, but the password was incorrect." ); 
    } 
}


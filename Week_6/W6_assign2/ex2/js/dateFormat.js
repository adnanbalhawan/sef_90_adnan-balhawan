var getDate = function() 
{
    var months = ["January", "February"
        , "March", "April"
        , "May", "June"
        , "July", "August"
        , "September", "October"
        , "November", "December"];

    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    var d = new Date();
    var day = days[d.getDay()];
    var hr = d.getHours();
    var min = d.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var ampm = hr < 12 ? "am" : "pm";
    var date = d.getDate();
    var month = months[d.getMonth()];
    var year = d.getFullYear();
    var x = document.getElementById("time");
    var format = day + ", " + month + " " + date + " " + year + " " + hr + ":" + min;

    return format;
};
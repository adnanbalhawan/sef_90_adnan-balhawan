var toDoApp = 
{
    escapeHTML: function(input) {
        return input
            .replace(/&/g, " ")
            .replace(/</g, " ")
            .replace(/>/g, " ")
            .replace(/"/g, " ")
            .replace(/'/g, " ");
    },

    addItem: function() 
    {
        var title = document.getElementById('title').value;
        var content = document.getElementById('content').value;

        if(!title || title == '' || !content || content=='')
        {
            document.getElementById('title').setAttribute('placeholder',
                 'ERROR, please fill the empty feilds');
            document.getElementById('content').setAttribute('placeholder','');
        }
        else
        {
            document.getElementById('title').value = '';
            document.getElementById('content').value = '';

            title = this.escapeHTML(title);
            content = this.escapeHTML(content);

            var task = {
                key: Date.now(),
                title: title,
                added: getDate(),
                content: content
            };

            localStorage.setItem(task.key, JSON.stringify(task));

            this.display(this.getAll());
        }        
    },

    getAll: function()
    {
        var dataArray = [];
        var keys = Object.keys(localStorage);
        var i = keys.length;

        while ( i-- ) 
        {
            var data = localStorage.getItem(keys[i]);
            data = JSON.parse(data);
            dataArray.push(data);
        }

        return dataArray;
    },

    display: function(dataArray)
    {
        var infoBox = document.getElementById('infoBox');
        infoBox.innerHTML='';

        for(var i = 0; i < dataArray.length; i++)
        {
            var entryDiv = document.createElement('div');
            entryDiv.setAttribute('class', 'entry');

            var task =  document.createElement('div');
            task.setAttribute('class', 'task');

            var title = document.createElement('h2');
            var titleText = document.createTextNode(dataArray[i].title);
            title.appendChild(titleText);
            task.appendChild(title);

            var timestamp = document.createElement('p');
            timestamp.setAttribute('class', 'timestamp');
            var added = document.createTextNode('added: '+dataArray[i].added);
            timestamp.appendChild(added);
            task.appendChild(timestamp);

            var description = document.createElement('p');
            var descriptionText = document.createTextNode(dataArray[i].content);
            description.appendChild(descriptionText);
            task.appendChild(description);
            entryDiv.appendChild(task);

            var seperator = document.createElement('div');
            seperator.setAttribute('class', 'delete');
            
            var button = document.createElement('button');
            button.setAttribute('onclick',
                "toDoApp.deleteEntry("+dataArray[i].key+")");
            var x = document.createTextNode('x');
            button.appendChild(x);
            seperator.appendChild(button);
            entryDiv.appendChild(seperator);

            infoBox.appendChild(entryDiv);
        }
    },

    deleteEntry: function(entryKey)
    {       
        localStorage.removeItem(entryKey);
        this.display(this.getAll());
    }
};

<?php

require_once 'aylien.conf.php';

/**
 *  getting the API Key and ID from conf file
 *  defined as follows:
 *
 *  define('APPLICATION_ID',   ID);
 *  define('APPLICATION_KEY',   KEY);
 *
 */

function call_api($endpoint, $parameters) 
{
  $ch = curl_init('https://api.aylien.com/api/v1/' . $endpoint);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/json',
    'X-AYLIEN-TextAPI-Application-Key: ' . APPLICATION_KEY,
    'X-AYLIEN-TextAPI-Application-ID: '. APPLICATION_ID
  ));
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
  $response = curl_exec($ch);
  return json_decode($response);
}

//get JSON from PSOT and decode it
$str_json = file_get_contents('php://input');
$str_json = json_decode($str_json);

$summarize = call_api('summarize', $str_json);

echo join("<br>", $summarize->sentences);

// $finalResult = implode(" ", $summarize->sentances);
// echo $finalResult;

?>

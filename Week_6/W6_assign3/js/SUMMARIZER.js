var SUMMARIZER =
{
    proxyURL: 'php/simple-proxy.php?url=',
    AylienAPI: 'php/AylienApi.php',

    title: '',
    parsing: '',
    summarization: '',
    content: '',

    URLregex: /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/,
    HPregex: /(<h(1|2|3) (.*?)>(.*?)<\/h(1|2|3)>|<p (.*?)>(.*?)<\/p>)/g,
    Hregex: /<h1 (.*?)>(.*?)<\/h1>/,
    removeTagsRegex: /(<(.*?)>|<\/(.*?))>/g,

    /**
     * get Article content
     */
    ArticleProcess: function(URL)
    {
        if(this.verifyURL(URL))
        {
            this.loaderAnimation();
            var requestURL = this.proxyURL+URL+'&mode=native';
            this.getContent(requestURL);
        }
    },

    /**
     * verfiying that the URL is valid and get content
     */
    verifyURL: function(URL)
    {
        if(!this.URLregex .test(URL))
        {
            alert('Please enter valid URL.');
        }
        else
        {
            return true;            
        }
    },

    /**
     * calling the proxy server to get the content of the URL
     * forked from:
     * https://developers.google.com/web/fundamentals/getting-started/primers/promises
     */
    getContent: function(URL)
    {    
        var req = new XMLHttpRequest();
        req.open('GET', URL);

        req.onreadystatechange = function () {
            if (req.status == 200 && req.readyState == 4) {
                SUMMARIZER.SummarizationProcess(req.responseText);         
            }
        };

        req.send(null);
    },

    /**
     * process of parsing and requesting summarization
     */
    SummarizationProcess: function(content)
    {
        this.parseContent(content, this.sendToAlyien);
    },

    /**
     * parsing HTML content
     * forked from: https://stackoverflow.com/questions/28899298/extract-the-text-out-of-html-string-using-javascript
     */
    parseContent: function(content, callback)
    {
        var filtered = content.match(this.HPregex);

        var span = document.createElement('span');

        span.innerHTML = filtered;
        var children = span.querySelectorAll('*');

        for(var i = 0 ; i < children.length ; i++) {
        if(children[i].textContent)
            children[i].textContent += ' ';
        else
            children[i].innerText += ' ';
        }

        var sentences_number = children.length / 0.3;
        if(sentences_number < 3)
        {
            sentences_number = 3;
        }
        else if (sentences_number > 10)
        {
            sentences_number = 10;
        }

        var parsing = [span.textContent || span.innerText].toString().replace(/ +/g,' ');
        var cleanupParsing = parsing.replace(parsing.match(/"/g),"'");
        var title = span.querySelector('h1').innerText;

        callback(title, cleanupParsing, sentences_number);
    },

    /**
     * make the Aylien API request
     */
    sendToAlyien: function(title, parsing, sentences_number)
    {
        var requestURL = 'php/AylienApi.php';

        var req = new XMLHttpRequest();
        req.open('POST', requestURL);
        req.setRequestHeader("Content-type", "application/json");

        req.onreadystatechange = function () {
            if (req.status == 200 && req.readyState == 4) {
                SUMMARIZER.display(title, req.responseText);         
            }
        };

        data = {
            text: parsing,
            title: title,
            sentences_number: sentences_number
        }

        req.send(JSON.stringify(data));
    },

    /**
     * displaying the summary after
     */
    display: function(title, summary)
    {
        //overwriting the animation
        document.getElementById('process').innerHTML = '';
        //adding the final response
        document.getElementById('info').innerHTML = 
            "<div id='textblock'>"+ 
            "   <h3>"+title+"</h3>"+
            "   <p>"+summary+"</p>"+
            "</div>";
    },

    /**
     * loading animation
     */
    loaderAnimation: function()
    {
        document.getElementById('process').innerHTML = '';
        document.getElementById('info').innerHTML = 
        "<div class='loader' title='7'>"
        +"  <svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'"
        +"      width='700px' height='50px' viewBox='0 0 24 30' style='enable-background:new 0 0 50 50;' xml:space='preserve'>"
        +"      <rect x='0' y='10' width='4' height='10' fill='#333' opacity='0.2'>"
        +"          <animate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0s' dur='0.6s' repeatCount='indefinite' />"
        +"          <animate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0s' dur='0.6s' repeatCount='indefinite' />"
        +"          <animate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0s' dur='0.6s' repeatCount='indefinite' />"
        +"      </rect>"
        +"          <rect x='8' y='10' width='4' height='10' fill='#333'  opacity='0.2'>"
        +"          <animate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0.15s' dur='0.6s' repeatCount='indefinite' />"
        +"           <animate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0.15s' dur='0.6s' repeatCount='indefinite' />"
        +"          <animate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0.15s' dur='0.6s' repeatCount='indefinite' />"
        +"      </rect>"
        +"          <rect x='16' y='10' width='4' height='10' fill='#333'  opacity='0.2'>"
        +"          <animate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0.3s' dur='0.6s' repeatCount='indefinite' />"
        +"          <animate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0.3s' dur='0.6s' repeatCount='indefinite' />"
        +"       <animate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0.3s' dur='0.6s' repeatCount='indefinite' />"
        +"      </rect>"
        +"  </svg>"
        +"</div>";
    },
};

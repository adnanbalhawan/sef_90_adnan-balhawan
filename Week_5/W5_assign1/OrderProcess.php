<?php

require_once 'MySQLWrap.php';


$film = new MySQLWrap();


if(!$_POST['films'])
{
    header ('LOCATION: Order.php');

}      
else 
{
    $filmInfo = $film->getFilmInfo($_POST['films']);
    $filmAvail = $film->getFilmAvail($_POST['films']);
}

require_once 'styling.html';

?>
<html>
    <body>
        <div id="formDiv">

            <title>
                SAKILA rental
            </title>

            <form action='' method='POST' id="myForm">
                <h1>
                    SAKILA film rental<br>
                    <span><br>Welcome to Sakila's website, please follow the 
                        steps below to rent a film!
                    </span>
                </h1>
                <section>
                    <span>2</span>
                    <b>About: <?php echo '  '.$_POST['films'] ?></b><br>
                </section>

                <table>
                <?php
                    foreach($filmInfo as $Info)
                    {
                        foreach($Info as $key => $data)
                        {
                            echo '<tr>' .
                                '<td>' . $key . ': </td>
                                <td>' . $data .'</td>
                                </tr>';
                        }
                    }
                ?>
                </table>
                <br>
                <?php
                // if(!$filmAvail)
                // {
                //     echo "<section>We're sorry, please feel 
                //         free to select another film while we 
                //         work on getting that one back 
                //         in stock!</section><br>";
                // }
                // else
                // {
                ?>
                    <table>
                        <tr>
                            <th> Copies left </th>
                            <th> Store Number </th>
                        <tr/>
                        
                        <?php
                            foreach($filmAvail as $Info)
                            {
                                echo '<tr>';
                                foreach($Info as $key => $data)
                                {
                                    echo '<td>' . $data .'</td>';   
                                }
                                echo '</tr>';
                            }
                        
                ?>
                </table>
                </form>

            <form action='finish.php' method='POST' id="myForm">
                    
                    <section>
                            To continue, please select your preferred store 
                            and enter your details, otherwise, select another film!<br>
                        <span>3</span> <b>Preferred store:</b><br>
                        <select name="store">
                            <?php 
                                foreach($filmAvail as $store) { 
                                echo "<option value=\"".$store['Store Number'] 
                                    ."\">".$store['Store Number'] ."</option>";
                                }
                            ?>
                        </select><br>
                        <span>4</span>
                        <b>Your details: </b><br>

                        <label>Please enter your full name and email address below</label>
                        Full name: 
                        <input type='text' name='name'  placeholder='john smith'><br>
                        Email: 
                        <input type="text" name="email" placeholder='john@smith.com'><br>

                    </section>
                    <?php 
                    // } 
                    ?>
                    <input type="submit" value="Rent" ><br>
            
            </form>

        </div>

    </body>
</html>


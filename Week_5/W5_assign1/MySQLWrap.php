<?php

require_once 'config.php';

class MySQLWrap
{
    private $dbCon;
    private $result;
    private $tempArray;
    private $sel;
    private $staff_id;
    private $inventory_id;
    private $film_id;
    private $s_id;
    private $c_id;
    private $i_id;
    private $f_id;

    function __construct ()
    {
        $config = new config();
        $cred = $config->getCred();

        $this->dbCon = new mysqli($cred['host'], 
            $cred['dbUser'], $cred['dbPw'], $cred['dbName']);

        if ($this->dbCon->connect_error)
        {
            die($this->dbCon->connect_error);
        }
    }

    function select ($sel)
    {
        unset($this->tempArray);

        $this->result = $this->dbCon->query($sel);

        while($row = $this->result->fetch_assoc())
        {
            $this->tempArray[] = $row;
        }
        
        $this->result->free();
        return $this->tempArray;
    }

    function getFilmsList()
    {

        $this->sel = "SELECT film_id, title FROM film;";
        
        return $this->select($this->sel);
    }

    function getFilmAvail ($film)
    {
        $this->sel = 
            "SELECT 
                COUNT(f.film_id) AS 'Available copies',
                store_id AS 'Store Number'
            FROM
                inventory AS i,
                film AS f
            WHERE
                f.film_id = i.film_id
                    AND f.title = '".$film
            ."' GROUP BY store_id;";

        return $this->select($this->sel);
    }

    function getFilmInfo ($film)
    {
        $this->sel = 
            "SELECT 
                description AS 'Film Description',
                release_year AS year,
                length,
                rating,
                rental_rate AS 'Rent Rate (USD)'
            FROM
                film
            WHERE
	            title = '".$film."';";

        return( $this->select($this->sel));
    }

    function rentFilm ($film, $store, $email)
    {
        $this->sel = 
            "SELECT 
                customer_id AS id 
            FROM customer 
            WHERE email = '".$email."';";

        $this->customer_id = $this->select($this->sel);

        if(!$this->customer_id)
        {
            return FALSE;
        }
        else
        {
            $this->sel =
                "SELECT staff_id FROM staff where store_id =".$store." ;";
            $this->staff_id = $this->select($this->sel);

            $this->sel =
                "SELECT film_id FROM film WHERE title = '".$film."';";
            $this->film_id = $this->select($this->sel);
            
            foreach($this->customer_id as $ar)
            {
                foreach($ar as $ids)
                {
                    $this->c_id = $ids;
                }
            }

            foreach($this->film_id as $ar)
            {
                foreach($ar as $ids)
                {
                    $this->f_id = $ids;
                }
            }

            foreach($this->staff_id as $ar)
            {
                foreach($ar as $ids)
                {
                    $this->s_id = $ids;
                }
            }

            $this->sel =
                "SELECT 
                    inventory_id
                FROM
                    inventory
                WHERE
                    store_id = ".$store." AND film_id = ".$f_id."1 limit 1;";
            $this->inventory_id = $this->select($this->sel);
            foreach($this->inventory_id as $ar)
            {
                foreach($ar as $ids)
                {
                    $this->i_id = $ids;
                }
            }

            $this->rent = "INSERT INTO rental
                (rental_date, inventory_id, customer_id, staff_id)
                VALUES
                ('". date('Y-m-d H:i:s')."', ".$this->i_id.', '.$this->c_id.', '.$this->s_id.');';
            $this->dbCon->query($this->rent);
            return TRUE;
        }
    

    }

    function __destruct ()
    {
        $this->dbCon->close();
    }

}


    
<?php

require_once 'styling.html';
require_once 'MySQLWrap.php';

$data = new MySQLWrap();
$films = $data->getFilmsList();
?>

<html>
<body>
    <div id="formDiv">

        <title>
            SAKILA rental
        </title>

        <form action='OrderProcess.php' method='POST' id="myForm">
            <h1>
                SAKILA film rental<br>
                <span><br>Welcome to Sakila's website, please follow the 
                    steps below to rent a film!
                </span>
            </h1>

            <section>
                <span>1</span>
                <b> Films: </b>
            </section>
                <label>Please select from the below list and click display info button</label>
                <input list="films" name="films" /></label>
                <datalist id='films'>

                    <?php
                        foreach($films as $title) { 
                        echo "<option value=\"".$title['title'] 
                            ."\">".$title['title']."</option>";
                        }
                    ?>
                </datalist>

            <br>
            <input type="submit" value="display info">

        </form>
    </div>

</body>
</html>



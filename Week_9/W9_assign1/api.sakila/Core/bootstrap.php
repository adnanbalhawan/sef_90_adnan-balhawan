<?php

class Bootstrap
{
    public static $controller;
    public static $param;

    public function __construct()
    {
        if($_SERVER['REQUEST_URI'] == '/' && 
            $_SERVER['REQUEST_METHOD'] == 'GET')
        {
            IndexController::home();
        }
        else
        {
            $this->decode();
        }
    }

    public function controllers()
    {
        switch($this->controller):
            case 'movies':
                MoviesController::index($this->db);
        endswitch;
    }

    public function decode()
    {
        $URI = $_SERVER['REQUEST_URI'];
        $URI = preg_replace('/[^.\/A-Za-z0-9]/', '', $URI);
        $args = preg_split('/[\/]/', $URI);
        if (count($args) > 2) 
        { 
            Self::$controller = Self::$controller = '/'.$args[1].'/';;
        }
        else
        {
            Self::$controller = Self::$controller = '/'.$args[1];
        }

        if (count($args) > 2) 
        { 
            Self::$param = $args[2];
        }

        $this->method();
    }

    public function method()
    {
        switch ($_SERVER['REQUEST_METHOD']):
            case 'GET':
                $this->call('get');
                break;
            case 'POST':
                $this->getData();
                $this->call('post');
                break;
            case 'PUT':
                $this->call('put');
                break;
            case 'DELETE':
                $this->call('delete');
                break;
        endswitch;
    }

    public function call($method)
    {
        if(isset(Route::$routes[$method][Self::$controller]))
        {
            call_user_func(Route::$routes[$method][Self::$controller]);
        }
        else
        {
             print_r(json_encode(
                ['code' => '404',
                'content' => 'Not Found']
            ));
        }
    }

    public function getData()
    {        
        //Receive the RAW post data.
        $content = trim(file_get_contents("php://input"));
        
        //Attempt to decode the incoming RAW post data from JSON.
        $decoded = json_decode($content, true);
        
        //If json_decode failed, the JSON is invalid.
        if(!is_array($decoded))
        {
            print_r(json_encode(
                ['code' => '400',
                'content' => 'Invalid JSON object']
            ));
        }
        else
        {
            Self::$param = $decoded;
        }
    }
}
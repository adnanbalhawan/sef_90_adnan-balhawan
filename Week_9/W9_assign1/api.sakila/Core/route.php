<?php

class Route
{
    public static $routes = array();

    public function add($method, $route, $destination)
    {
        preg_match('/\{([^}]+)\}/', $route, $matches);

        if(isset($matches[0]))
        {
            preg_match('/\/([^}]+)\//', $route, $route);
            $route = $route[0];
        }

        Self::$routes[$method][$route] = $destination;
    }
}
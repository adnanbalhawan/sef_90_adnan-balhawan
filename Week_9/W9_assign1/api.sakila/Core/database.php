<?php

require_once ".config";

class Database
{
    private static $connection;
    private static $tempArray;
    private static $result;

    function __construct()
    {
        self::$connection = new mysqli(host, user, password, database);

        if (self::$connection->connect_error)
        {
            die(self::$connection->connect_error);
        }
    }

    function select($sel)
    {
        self::$result = self::$connection->query($sel);

        while($row = self::$result->fetch_assoc())
        {
            self::$tempArray[] = $row;
        }
        
        self::$result->free();
        
        return self::$tempArray;
    }

    function insert($sel)
    {
        if(self::$connection->query($sel))
        {
        }
        else
        {
            echo(self::$connection->error);
        }
    }
    
    function __destruct()
    {
        self::$connection->close();
    }
}
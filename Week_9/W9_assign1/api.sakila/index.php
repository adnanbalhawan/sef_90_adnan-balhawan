<?php

require_once "Core/database.php";
require_once "Core/bootstrap.php";
require_once "Core/route.php";

require_once "api.php";

require_once "Models/Movie.php";

require_once "Controllers/MoviesController.php";
require_once "Controllers/IndexController.php";

$db = new Database();
$route = new Bootstrap();
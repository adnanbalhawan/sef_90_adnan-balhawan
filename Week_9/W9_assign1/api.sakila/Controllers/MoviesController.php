<?php

class MoviesController
{
    public function index()
    {
        Movie::all();
    }

    public function getFilm()
    {
        Movie::find(Bootstrap::$param);
    }

    public function add()
    {
        $data = Bootstrap::$param;

        if(isset($data['title']) && 
            isset($data['language_id']))
        {
            $feilds =  array();
            array_push($feilds, $data['title'].',');
            array_push($feilds, $data['language_id']);
            array_push($feilds, isset($data['description'])?'description,':'');
            array_push($feilds, isset($data['release_year'])?'release_year, ':'');
            array_push($feilds, isset($data['original_language_id'])? 'original_language_id, ' : '');
            array_push($feilds, isset($data['rental_duration'])? 'rental_duration, ' : '');
            array_push($feilds, isset($data['rental_rate'])? 'rental_rate, ' : '');
            array_push($feilds, isset($data['length'])? 'length, ' : '');
            array_push($feilds, isset($data['replacement_cost'])? 'replacement_cost, ' : '');
            array_push($feilds, isset($data['rating'])? 'rating, ' : '' .'');
            array_push($feilds, isset($data['special_features'])?'special_features, ':'');            


            $values =  array();
            array_push($values, "'" . $data['title'] . "',");
            array_push($values, $data['language_id']);
            array_push($values, isset($data['description'])? "'".$data['description']."'," : '');
            array_push($values, isset($data['release_year'])? $data['release_year'] . ',': '');
            array_push($values, isset($data['original_language_id'])? $data['original_language_id'] . ',': ''); 
            array_push($values, isset($data['rental_duration'])? $data['rental_duration'] . ',' : '');
            array_push($values, isset($data['rental_rate'])? $data['rental_rate'] . ',' : '');
            array_push($values, isset($data['length'])? $data['length'] . ',' : '');
            array_push($values, isset($data['replacement_cost'])? $data['replacement_cost'] . ',' : '');
            array_push($values, isset($data['rating'])? $data['rating'] . ',' : '');
            array_push($values, isset($data['special_features'])? $data['special_features'] : '' .');');            

            $query = "INSERT INTO film (" 
                . implode('', $feilds)
                . ') VALUES (' 
                . implode('', $values);

            Movie::create($query);
        }
        else
        {
            print_r(json_encode(
                ['code' => '400',
                'content' => 'Invalid Json']
            ));
        }       
    }
}
<?php

/**
 * Here we can define custom routes
 * example: Route::add("method", "route", "ClassName::MethodName")
 */

Route::add('get', '/', 'IndexController::home');

Route::add('get','/movies', 'MoviesController::index');

Route::add('get','/movies/{id}', 'MoviesController::getFilm');

Route::add('post','/movies', 'MoviesController::add');
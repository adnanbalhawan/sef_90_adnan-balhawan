<?php

class Movie
{
    public function all()
    {
        $query = "SELECT 
                        *
                    FROM
                        film";

        
        $data = Database::select($query);

        print_r(json_encode([
            'code' => 200,
            'content' => $data])
        );
    }

    public function find($id)
    {
        $query = "SELECT 
                        *
                    FROM
                        film
                    WHERE
                        film_id = ".$id;
        
        // print($query);
        $data = Database::select($query);

        print_r(json_encode([
            'code' => 200,
            'content' => $data])
        );
    }

    public function create($data)
    {
        // print_r($data);

        if(Database::insert($data))
        {
            print_r(json_encode([
                'code' => 200,
                'content' => 'OK'])
            );
        }
        else 
        {
            print_r(json_encode([
                'code' => 500,
                'content' => 'Internal Server Error'])
            );
        }    
    }
}
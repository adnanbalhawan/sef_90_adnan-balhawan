/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_1 Ex1
 * Date: June 10th, 2017
 * Last Modified: June 11th, 2017
 */

DROP SCHEMA IF EXISTS FinanceDB;
CREATE SCHEMA FinanceDB;
USE FinanceDB;

--
-- Table structure for table 'FiscalYearTable'
--

CREATE TABLE FiscalYearTable (
    fiscal_year YEAR NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    PRIMARY KEY (fiscal_year)
);

DROP TRIGGER IF EXISTS `FinanceDB`.`FiscalYearTable_BEFORE_INSERT`;

DELIMITER $$
USE `FinanceDB`$$
CREATE DEFINER=`adnan`@`localhost` TRIGGER `FinanceDB`.`FiscalYearTable_BEFORE_INSERT` 
BEFORE INSERT ON `FiscalYearTable` 
FOR EACH ROW
BEGIN
	IF NEW.end_date <= NEW.start_date
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'error in date range';
	end if;
        
	if extract(month from new.start_date) != 10 
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'start date is invalid';
	end if;
    
    if extract(day from new.start_date) != 1
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'start date is invalid';
	end if;

	if extract(month from new.end_date) != 9
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'end date is invalid';
	end if;
    
    if extract(day from new.end_date) != 30
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'end date is invalid';
	end if;
        
	if extract(year from new.end_date) - extract(year from new.start_date) != 1
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'invalid date';
   end if;
   
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `FinanceDB`.`FiscalYearTable_BEFORE_DELETE`;

DELIMITER $$
USE `FinanceDB`$$
CREATE DEFINER = CURRENT_USER TRIGGER `FinanceDB`.`FiscalYearTable_BEFORE_DELETE` BEFORE DELETE ON `FiscalYearTable` FOR EACH ROW
BEGIN
	SIGNAL SQLSTATE '45000' 
	SET MESSAGE_TEXT = 'operation canceled, deleting is not allowed'; 
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `FinanceDB`.`FiscalYearTable_BEFORE_UPDATE`;

DELIMITER $$
USE `FinanceDB`$$
CREATE DEFINER = CURRENT_USER TRIGGER `FinanceDB`.`FiscalYearTable_BEFORE_UPDATE` BEFORE UPDATE ON `FiscalYearTable` FOR EACH ROW
BEGIN
	SIGNAL SQLSTATE '45000' 
	SET MESSAGE_TEXT = 'operation canceled, updating is not allowed';
END$$
DELIMITER ;

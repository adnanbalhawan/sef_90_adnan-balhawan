/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_1 Ex2
 * Date: June 10th, 2017
 * Last Modified: June 11th, 2017
 */

USE HospitalRecords;

--
-- Dumping data for table AnestProcedures
--

INSERT INTO AnestProcedures VALUES (1, 'Albert', '8:00', '11:00'),
(2, 'Albert', '9:00', '13:00'),
(3, 'Kamal', '8:00', '13:30'),
(4, 'Kamal', '9:00', '15:30'),
(5, 'Kamal', '10:00', '11:30'),
(6, 'Kamal', '12:30', '13:30'),
(7, 'Kamal', '13:30', '14:30'),
(8, 'Kamal', '18:30', '19:00');
/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_1 Ex2
 * Date: June 10th, 2017
 * Last Modified: June 11th, 2017
 */

use HospitalRecords;

select
   id as proc_id, max(total) AS max_inst_count
from
(
	select p1.proc_id as id, count(p2.proc_id) as total
	from AnestProcedures as p1, AnestProcedures as p2, AnestProcedures as p3
	WHERE p2.anest_name = p1.anest_name
	and p3.anest_name = p1.anest_name

	and p1.start_time <= p2.start_time
	and p2.start_time < p1.end_time

	and p3.start_time <= p2.start_time
	and p2.start_time < p3.end_time

	group by p1.proc_id, p2.proc_id
)x
group by id;

	

/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_1 Ex2
 * Date: June 10th, 2017
 * Last Modified: June 11th, 2017
 */

DROP SCHEMA IF EXISTS HospitalRecords;
CREATE SCHEMA HospitalRecords;
USE HospitalRecords;

--
-- Table structure for table 'HospitalRecords'
--

CREATE TABLE AnestProcedures (
	proc_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    anest_name varchar(45) NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    PRIMARY KEY (proc_id)
);
/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_5 Return the first and last names of actors 
-- who played in a film involving a “Crocodile” and a “Shark”, along
-- with the release year of the movie, sorted by the actors’ last names.

select a.first_name, a.last_name, f.release_year
from actor as a, film as f, film_actor as af
where a.actor_id = af.actor_id
and f.film_id = af.film_id
and description like '%crocodile%' or '%shark%'
order by a.last_name;
/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_2 What are the top 3 languages for movies released in 2006?

select l.name as Language, count(f.language_id) as times_used
from film as f, language as l
where 
l.language_id = f.language_id
and f.release_year=2006
group by l.name, f.language_id
limit 3;
/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_8 Get the total and average values of 
-- rentals per month per year per store.

select s.store_id, avg(p.amount), 
monthname(p.payment_date) as Month, 
extract(year from payment_date) as Year
from payment as p, store as s, customer as c
where s.store_id = c.store_id
and c.customer_id = p.customer_id
group by 
monthname(p.payment_date), 
extract(year from p.payment_date),
s.store_id
order by Year, s.store_id, Month desc;
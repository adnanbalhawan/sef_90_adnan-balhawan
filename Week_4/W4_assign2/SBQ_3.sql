/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

 -- SBQ_3 What are the top 3 countries from which customers are originating?

select co.country, count(co.country_id) as number_of_customers
from customer as cu, address as ad, city as ci, country as co
where cu.address_id = ad.address_id
and ad.city_id = ci.city_id
and ci.country_id = co.country_id
group by co.country, co.country_id
order by count(co.country_id) desc
limit 3;
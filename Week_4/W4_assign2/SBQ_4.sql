/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_4 Find all the addresses where the second address 
-- is not empty (i.e., contains some text), and return these
-- second addresses sorted.

select * from address where address2 <>''
order by address_id asc;
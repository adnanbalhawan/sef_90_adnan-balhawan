/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_9 Get the top 3 customers who rented 
-- the highest number of movies within a given year.

select c.customer_id, c.first_name, c.last_name, count(i.film_id),
extract(year from r.rental_date) as rental_year
from customer as c, inventory as i, rental as r 
where r.inventory_id = i.inventory_id
and c.customer_id = r.customer_id
and extract(year from r.rental_date) = 2005
group by rental_year, c.customer_id
order by count(i.film_id) desc
limit 3;

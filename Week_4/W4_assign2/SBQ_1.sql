/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */
 
use sakila;

-- SBQ_1 What is the total number of movies per actor?

select concat(a.first_name, ' ', a.last_name) as actor_name, 
count(af.film_id) as number_of_films
from actor as a, film_actor as af
where a.actor_id = af.actor_id
group by a.actor_id;
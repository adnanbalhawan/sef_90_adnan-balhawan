/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_6 Find all the film categories in which 
-- there are between 55 and 65 films. 
-- Return the names of thesecategories 
-- and the number of films per category, 
-- sorted by the number of films. If there are no categories
-- between 55 and 65, return the highest available counts.

select c.name, count(fc.film_id)
from film_category as fc, category as c, film as f
where f.film_id = fc.film_id
and c.category_id = fc.category_id 

group by c.name
having  case
            when (count(fc.film_id) between 55 and 65) = null
            then count(fc.film_id)
            else (count(fc.film_id) between 55 and 65)
        END
order by count(fc.film_id) desc;
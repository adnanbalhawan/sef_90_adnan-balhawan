/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_2
 * Date: June 11th, 2017
 */

 USE sakila;

-- SBQ_7 Find the names (first and last) 
-- of all the actors and costumers whose 
-- first name is the same as the first
-- name of the actor with ID 8. 
-- Do not return the actor with ID 8 himself. 
-- Note that you cannot use the name of
-- the actor with ID 8 as a constant (only the ID). 
-- There is more than one way to solve this question, but you
-- need to provide only one solution.

select * from actor;

select first_name, last_name from actor
where (select first_name from actor where actor_id = 8) = first_name
union
select first_name, last_name from customer
where first_name = (select first_name from actor where actor_id = 8);
/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_3
 * Date: June 12th, 2017
 */

DROP SCHEMA IF EXISTS LegalClaims;
CREATE SCHEMA LegalClaims;
USE LegalClaims;

-- Building table structure

CREATE TABLE claims (
    claim_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    patient_name VARCHAR(45) NOT NULL,
    PRIMARY KEY (claim_id)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE defendants (
    def_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    claim_id SMALLINT NOT NULL,
    def_name VARCHAR(45) NOT NULL,
    PRIMARY KEY (def_id)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE claimStatusCodes (
    claim_status CHAR(2) NOT NULL,
    claim_status_desc VARCHAR(45) NOT NULL,
    claim_seq SMALLINT NOT NULL,
    PRIMARY KEY (claim_status)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE legalEvents (
    event_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    claim_id SMALLINT NOT NULL,
    def_name VARCHAR(45) NOT NULL,
    claim_status CHAR(2) NOT NULL,
    change_date DATE NOT NULL,
    PRIMARY KEY (event_id)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_3
 * Date: June 12th, 2017
 */

USE LegalClaims;

SELECT 
    cl.claim_id, cl.patient_name, csc1.claim_status
FROM
    claims AS cl,
    claimStatusCodes AS csc1
WHERE
    csc1.claim_seq IN (SELECT 
            MIN(csc2.claim_seq)
        FROM
            claimStatusCodes AS csc2
        WHERE
            csc2.claim_seq IN (SELECT 
                    MAX(csc3.claim_seq)
                FROM
                    legalEvents AS le,
                    claimStatusCodes AS csc3
                WHERE
                    le.claim_status = csc3.claim_status
                        AND le.claim_id = cl.claim_id
                GROUP BY le.def_name))
                ORDER BY cl.claim_id ASC;
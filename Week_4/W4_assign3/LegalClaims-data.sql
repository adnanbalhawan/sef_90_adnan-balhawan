/**
 * Author: Adnan Balhawan
 * SE Factory - Week_4 Assignment_3
 * Date: June 12th, 2017
 */

USE LegalClaims;

--
-- Dumping data for table claims
--

INSERT INTO claims VALUES 
(1, 'Bassem Dghaidi'), 
(2, 'Omar Breidi'),
(3, 'Marwan Sawwan');

--
-- Dumping data for table claims
--

INSERT INTO defendants VALUES 
(null, 1, 'Jean Skaff'), 
(null, 1, 'Elie Meouchi'),
(null, 1, 'Radwan Sameh'),
(null, 2, 'Joseph Eid'),
(null, 2, 'Paul Syoufi'),
(null, 2, 'Radwan Sameh'),
(null, 3, 'Issam Awwad');


--
-- Dumping data for table claimStatusCodes
--

INSERT INTO claimStatusCodes VALUES 
('AP', 'Awaiting review panel', 1),
('OR', 'Panel opinion rendered', 2),
('SF', 'Suit filed', 3),
('CL', 'Closed', 4);


--
-- Dumping data for table legalEvents
--

INSERT INTO legalEvents 
(claim_id, def_name, claim_status, change_date)
VALUES 
(1, 'Jean Skaff', 'AP', '2016-01-01'),
(1, 'Jean Skaff', 'OR', '2016-02-02'),
(1, 'Jean Skaff', 'SF', '2016-03-01'),
(1, 'Jean Skaff', 'CL', '2016-04-01'),
(1, 'Radwan Sameh', 'AP', '2016-01-01'),
(1, 'Radwan Sameh', 'OR', '2016-02-02'),
(1, 'Radwan Sameh', 'SF', '2016-03-01'),
(1, 'Elie Meouchi', 'AP', '2016-01-01'),
(1, 'Elie Meouchi', 'OR', '2016-02-02'),
(2, 'Radwan Sameh', 'AP', '2016-01-01'),
(2, 'Radwan Sameh', 'OR', '2016-02-01'),
(2, 'Paul Syoufi', 'AP', '2016-01-01'),
(3, 'Issam Awwad', 'AP', '2016-01-01');


<?php

/**
 * Author: Adnan Balhawan
 * SE Factory - Week_3 Assignment_1
 * Date: June 5th, 2017
 * Last Modified: June 6th, 2017
 *
 * Class Root
 * contains all functions needed to perform database functions
 */

 class Root
 {

     private $activeUser;
     private $rootDir;
     private $iterator;
     private $selDB;
     private $selectedTable;

     public $help = "This script is desinged as a simple database management system "
        . "to store information and create database files\n"
        . "Functions included:\n\n"
        . "-- 1: CREATE,DATABASE,'database name' -> creates a database\n"
        . "-- 2: DELETE,DATABASE,'database name' -> deletes a database\n"
        . "-- 3: USE,'database name','table name' -> selects working db and table\n"
        . "-- 4: USE,'database name' -> selects working db \n"
        . "-- 5: CREATE,TABLE,'table name',columns (seperated by a comma)\n"
        . "     -> creates a table with specified columns names\n"
        . "-- 6: ADD,(information for a raw separated by a comma)\n"
        . "     -> adds information to a table\n"
        . "-- 7: GET,(ID number) -> retreieves a raw from a selected table\n"
        . "-- 8: DELETE,TABLE,'table name' -> deletes a table from a database\n"
        . "-- 9: DELETE,ROW,'ID number' -> deletes a row from the working table\n"
        . "--10: PRINT -> print an array of current working table\n"
        . "--11: PRINTSTR -> print current folder structure\n"
        . "--12: EXIT -> terminate the script\n";

     function __construct ()
     {
        exec('whoami', $this->activeUser);
        //$this->rootDir = '/home/'.$this->activeUser[0].'/root/';

        $this->rootDir = readline("please enter full diretory path: \n");

        if (substr($this->rootDir, -1) !== '/')
        {
            $this->rootDir = $this->rootDir.'/';
        }         
            
        $this->checkRootFolder();

        $this->iterator = new RecursiveIteratorIterator
        (new RecursiveDirectoryIterator($this->rootDir));

        $this->getFileStructure();

     }

     /**
      * Undocumented function
      *
      * @return void
      */
     function checkRootFolder ()
     {
        if (file_exists($this->rootDir)) 
        {
            echo "Selected Root Directory: " . $this->rootDir . " \n";
        } 
        else 
        {
            mkdir ($this->rootDir);
            echo "root directory doesn't exist, a new "
                . "root has been created.\n"
                . "Path: " . $this->rootDir;
        }

     }

     /**
      * Undocumented function
      *
      * @return void
      */
     function getPath ()
     {
         return $this->rootDir;
     }

     /**
      * Undocumented function
      *
      * @param [type] $DBname
      * @return void
      */
     function checkDB ($DBname)
     {
         return is_dir($this->rootDir . $DBname);
     }

     /**
      * Undocumented function
      *
      * @return void
      */
     function getFileStructure ()
     {

        $database = array();
        $files = array();
        $databaseDir = scandir($this->rootDir);

        foreach($this->iterator as $item)
        {
            if(basename($item) != '.' and basename($item) != '..')
            {
                $database[] = dirname($item);
            }
        }

        $database = array_unique($database);

        foreach($database as $dbKey => $db)
        {
            $dbKey = array();
            $DBiterator = new RecursiveIteratorIterator
            (new RecursiveDirectoryIterator($db));
            
            foreach($DBiterator as $files)
            {
                if(basename($files) != '.' and basename($files) != '..')
                {
                    $dbKey[] = basename($files);
                }
            }

        }

     }

     /**
      * Undocumented function
      *
      * @return void
      */
     function printFileStructure()
     {

        $database = array();
        $files = array();
        $databaseDir = scandir($this->rootDir);

        foreach($this->iterator as $item)
        {
            if(basename($item) != '.' and basename($item) != '..')
            {
                $database[] = dirname($item);
            }
        }

        $database = array_unique($database);

        foreach($database as $dbKey => $db)
        {
            $dbKey = array();
            $DBiterator = new RecursiveIteratorIterator
            (new RecursiveDirectoryIterator($db));
            
            foreach($DBiterator as $files)
            {
                if(basename($files) != '.' and basename($files) != '..')
                {
                    $dbKey[] = basename($files);
                }
            }

            echo "\nDatabase name: " . basename($db) . " , " . count($dbKey) . " table(s)\n"
                . "Tables available: " ;

            foreach($dbKey as $table)
            {
                echo $table.", ";
            }
            echo "\n";

        }

     }

     /**
      * Undocumented function
      *
      * @param [type] $DBname
      * @return void
      */
     function CreateDB ($DBname)
     {
         mkdir ($this->rootDir.$DBname.'.db');
         echo "\nCREATED\n\n";
         $this->getFileStructure($this->iterator);
     }

     /**
      * Undocumented function
      *
      * @param [type] $DBname
      * @return void
      */
     function DeleteDB ($DBname)
     {
         rmdir ($this->rootDir . $DBname);
         echo "\nDELETED\n\n";
         //$this->getFileStructure($this->iterator);
     }

     /**
      * Undocumented function
      *
      * @param [type] $DBname
      * @return void
      */
     function SelectDB($DBname)
     {
         if(in_array($DBname, $this->database))
         {
             $this->selDB = $DBname;
         }
         
     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @param [type] $columns
      * @return void
      */
     function CreateTable ($selDB, $selTable, $columns)
     {
        shell_exec('touch '.$this->rootDir.$selDB.'/'.$selTable);
        $this->addRecord($selDB, $selTable, $columns);
     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @param [type] $columns
      * @return void
      */
     function addRecord ($selDB, $selTable, $columns)
     {
        $handle = fopen($this->rootDir.$selDB.'/'.$selTable, 'a');

        fputcsv($handle, $columns);
        fclose($handle);
     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @return void
      */
     function DeleteTable ($selDB, $selTable)
     {
        rmdir($this->rootDir . $this->selDB . '/' . $selTable);
        echo "\nDELETED\n\n";
        $this->getFileStructure($this->iterator);
     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @return void
      */
     function loadCSVintoArray ($selDB, $selTable)
     {
        echo $this->rootDir.$selDB.'/'.$selTable;
        if(($handle = fopen($this->rootDir.$selDB.'/'.$selTable, 'r')) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
            {
                $datafromcsv[] = $data;
            }
        }
        fclose($handle);
        return $datafromcsv;
     }

     function printTable ($datafromcsv)
     {
         print_r($datafromcsv);
     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @param [type] $inquiry
      * @return void
      */
     function deleteRecord ($selDB, $selTable, $inquiry)
     {
        $datafromcsv = $this->loadCSVintoArray($selDB, $selTable);
        file_put_contents($this->rootDir.$selDB.'/'.$selTable,"");
        $status = FALSE;

        foreach($datafromcsv as $data)
        {
            if($data[0] == $inquiry)
            {
                $status = TRUE;
                unset($data);
            }
            else
            {
                $this->addRecord($selDB,$selTable, $data);
            }

        }

        if($status == FALSE)
        {
            echo "No results found, try again.";
        }

     }

     /**
      * Undocumented function
      *
      * @param [type] $selDB
      * @param [type] $selTable
      * @param [type] $inquiry
      * @return void
      */
     function getRecord ($selDB, $selTable, $inquiry)
     {
        $datafromcsv = $this->loadCSVintoArray($selDB, $selTable);
        $status = FALSE;

        foreach($datafromcsv as $data)
        {
            if($data[0] == $inquiry)
            {
                $status = TRUE;
                foreach($data as $rawdata)
                {

                    if(end($data) == $rawdata)
                    {
                        echo "\"" . $rawdata."\"\n";
                    }
                    else
                    {
                        echo "\"" . $rawdata . "\",";
                    }
                }
            }
        }

        if($status == FALSE)
        {
            echo "No results found, try again.";
        }

     }
 }

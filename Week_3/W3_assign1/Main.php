#!/usr/bin/php
<?php

/**
 * Author: Adnan Balhawan
 * SE Factory - Week_3 Assignment_1
 * Date: June 5th, 2017
 * Last Modified: June 6th, 2017
 * 
 * Main file to run the script
 */

require_once 'Root.php';

echo "program started!\n"
   . "type 'help' to get the list of comands available. \n"
   . "or start running commands directly.\n"
   . "NOTICE -- don't use '.db' when entering a database name\n";

$root = new Root;

$selDB = null;
$selTable = null;

/**
 * loop through commands to execute functions
 */
while (1)
{
    echo "\n";
    $command = readline('-> ');
    $command = explode(',', $command);
    $columns = array();

    switch ($command[0]):

        case 'help':
        case 'HELP';
            echo $root->help;
            break;

        case 'USE':
            if($root->checkDB($selDB) == 0)
            {
                echo 'invalid database folder';
                break;
            }
            else
            {
                $selDB = $command[1] . '.db';
            }

            if(array_key_exists(2,$command))
            {
                $selTable = $command[2];
            }
            else
            {
                $selTable = null;
            }
            break;

        case 'CREATE':

            switch ($command[1]):
                case 'DATABASE':
                    $root->createDB($command[2]);
                    $selDB = $command[2].'.db';
                    break;
                case 'TABLE':
                    if($selDB == null)
                    {
                        echo "please select database to use "
                            . "first, command: USE,'db name'";
                        break;
                    }
                    if(array_key_exists(3,$command))
                    {
                        for ($i = 3; $i < count($command); $i++)
                        {
                            $columns[] = $command[$i];
                        }
                        $root->createTable($selDB, $command[2], $columns);
                        echo $command[2] . " CREATED";
                    }
                    else
                    {
                        echo "invalid input";
                    }
                        
                    break;
                default:
                    echo "invalid entry, try again";
            endswitch;

            break;

        case 'DELETE':

            switch ($command[1]):
                case 'DATABASE':
                    $root->DeleteDB($command[2]);
                    echo $command[2] . " DELETED";
                    $selDB = null;
                    break;

                case 'ROW':
                    if($selDB == null)
                    {
                        echo "error, please select database to use"
                            . ", command: USE,'db name'";
                        break;
                    }
                    else if($selTable == null)
                    {
                        echo "please select a table to use from ". $selDB
                            . ", command: USE 'db name','table name";
                        break;
                    }
                    else
                    {
                        $root->deleteRecord($selDB, $selTable, $command[2]);
                    }
                    break;
                default:
                    echo "invalid entry, try again";
            endswitch;

            break;

        case 'GET':
            $root->getRecord($selDB, $selTable, $command[1]);
            break;
        case 'print';
        case 'PRINT':
            $data = $root->loadCSVintoArray($selDB, $selTable);
            $root->printTable($data);
            break;
        case 'PRINTSTR':
            echo "note that this method doesnt print empty database folders\n";
            $root->printFileStructure();
            break;
        case 'ADD':
            for ($i = 3; $i < count($command); $i++);
            {
                $columns[] = $command[$i];
            }
            $root->addRecord($selDB, $selTable, $columns);
            break;
        case 'exit':
        case 'EXIT':
            exit;
        default:
            echo "invalid entry, try again";
    endswitch;

}
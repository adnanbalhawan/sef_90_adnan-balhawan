var htmlrender =
{
  type: '',

  main: (data) =>
  {
    $('.emptyList').remove();
    switch (htmlrender.type)
    {
      case 'cards':
        htmlrender.render.cards(data);
        break;
      case 'collapse':
        htmlrender.render.collapse(data);
        break;
      case 'table':
        htmlrender.render.table(data);
        break;
      default:
        htmlrender.render.cards(data);
    }

    $('.dropdown-button').dropdown(
      {
        inDuration: 300,
        outDuration: 500,
        constrainWidth: false,
        hover: true,
        belowOrigin: true,
      }
    );
    $('.tooltipped').tooltip({delay: 50});
  },

  render:
  {
    cards: (data) =>
    {
      if(document.querySelector('.tasksColumn') == null)
      {
        $('#task-container').append(
          `<div class='col s12 m12 cards-container' id="tasksColumn"></div>`);
      }
      completed = '';
      style = '';
      sprintButton ='';
      edit ='';
      if(data.status == 'completed')
      {
        style = 'green accent-4';
      }
      else {
        style = 'blue lighten-1';
        completed =
        `<li>
          <a href='#' class="completed tooltipped" data-position="bottom" data-delay="50"
            data-tooltip="mark completed" id='${data.key}'><i class='material-icons'>done</i></a>
        </li>`;
        sprintButton =
        `<a href='#' class="sprint tooltipped" data-position="bottom" data-delay="50"
          data-tooltip="sprint" id='${data.key}'><i class='material-icons' style='font-size: 50px'>access_time</i></a>`;
        edit =
        `<li class='icons'>
          <a href='#' class="edit tooltipped" data-position="bottom" data-delay="50"
            data-tooltip="edit" id='${data.key}'><i class='material-icons'>edit</i></a>
        </li>`;
      }
      $('#tasksColumn').append(
        `<div class='card hoverable task ${style}' id='card${data.key}'>
          <div class='card-content white-text icons'>
            <h5><b>${data.title}</b></h5>
            <div>
              <a class="tooltipped" data-position="bottom" data-delay="50"
                data-tooltip="${new Date(data.timestamp)}">
                <small class='timestamp right-align' id='${data.timestamp}'>
                  ${date.format(new Date(data.timestamp))}
                </small>
              </a>
            </div>
            ${sprintButton}
            <a class='dropdown-button' href='#' data-activates='actions${data.key}'><i class="material-icons small">arrow_drop_down</i></a>
            <ul id='actions${data.key}' class='dropdown-content task'>
            ${edit}
              <li class='icons'>
                <a href='#' class="archive tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="archive" id='${data.key}'><i class='material-icons'>archive</i></a>
              </li>
              <li class='icons'>
                <a href='#' class="delete tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="delete" id='${data.key}'><i class='material-icons'>delete</i></a>
              </li>
              ${completed}
            </ul>
          </div>
        </div>`);
    },

    collapse: (data) =>
    {
      if(document.querySelector('#taskCollapse') == null)
      {
        $('#task-container').append(
          `<ul class="col s6 m12 collapsible popout" id='taskCollapse' data-collapsible="accordion"></ul>`);
        $('.collapsible').collapsible();
      }
      completed = '';
      style = '';
      sprintButton = '';
      edit = '';
      if(data.status == 'completed')
      {
        style = 'green accent-4'
      }
      else {
        style = 'red'
        completed =
        `<li>
          <a href='#' class="completed tooltipped" data-position="bottom" data-delay="50"
            data-tooltip="mark completed" id='${data.key}'><i class='material-icons'>done</i></a>
        </li>`;
        sprintButton =
        `<a href='#' class="sprint tooltipped" data-position="bottom" data-delay="50"
          data-tooltip="sprint" id='${data.key}'><i class='material-icons' style='font-size: 50px'>access_time</i></a>`;
        edit =
        `<li class='icons'>
          <a href='#' class="edit tooltipped" data-position="bottom" data-delay="50"
            data-tooltip="edit" id='${data.key}'><i class='material-icons'>edit</i></a>
        </li>`;
      }
      $('#taskCollapse').append(
        `<li class='task' id='card${data.key}'>
          <div class="collapsible-header ${style}">${data.title}</div>
          <div class="collapsible-body white">
            <div>
              <a class="tooltipped" data-position="bottom" data-delay="50"
                data-tooltip="${new Date(data.timestamp)}">
                <small class='timestamp right-align' id='${data.timestamp}'>
                  ${date.format(new Date(data.timestamp))}
                </small>
              </a>
            </div>
            ${sprintButton}
            <a class='dropdown-button' href='#' data-activates='actions${data.key}'><i class="material-icons small">arrow_drop_down</i></a>
            <ul id='actions${data.key}' class='dropdown-content task'>
              ${edit}
              <li class='icons'>
                <a href='#' class="archive tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="archive" id='${data.key}'><i class='material-icons'>archive</i></a>
              </li>
              <li class='icons'>
                <a href='#' class="delete tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="delete" id='${data.key}'><i class='material-icons'>delete</i></a>
              </li>
              ${completed}
            </ul>
          </div>
        </li>`);
    },

    table: (data) =>
    {
      if(document.querySelector('#tasksTable') == null)
      {
        $('#task-container').append(
          `<table class='bordered' id='tasksTable'>
            <tbody></tbody>
          </table>`);
      }
      completed = '';
      if(data.status == 'completed')
      {
        style = 'background: #69f0ae;'
      }
      else {
        style = 'background: #ffeb3b;'
        completed =
        `<li>
          <a href='#' class="completed tooltipped" data-position="bottom" data-delay="50"
            data-tooltip="mark completed" id='${data.key}'><i class='material-icons'>done</i></a>
        </li>`;
      }
      $('#tasksTable > tbody').append(
        `<tr class='task-table' id='card${data.key}' style='${style}'>
          <td class='title'>${data.title}</td>
          <td>
            <a class='dropdown-button table-button right' href='#' data-activates='actions${data.key}'><i class="material-icons small">arrow_drop_down_circle</i></a>
            <ul id='actions${data.key}' class='dropdown-content task'>
            <li class='icons'>
              <a href='#' class="sprint tooltipped" data-position="bottom" data-delay="50"
                data-tooltip="sprint" id='${data.key}'><i class='material-icons icons' style='font-size: 50px'>access_time</i></a>
            </li>
              <li class='icons'>
                <a href='#' class="edit tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="edit" id='${data.key}'><i class='material-icons'>edit</i></a>
              </li>
              <li class='icons'>
                <a href='#' class="archive tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="archive" id='${data.key}'><i class='material-icons'>archive</i></a>
              </li>
              <li class='icons'>
                <a href='#' class="delete tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="delete" id='${data.key}'><i class='material-icons'>delete</i></a>
              </li>
              ${completed}
              <li>
                <a class="tooltipped" data-position="bottom" data-delay="50"
                  data-tooltip="${new Date(data.timestamp)}">
                  <small class='timestamp' id='${data.timestamp}'>
                    ${date.format(new Date(data.timestamp))}
                  </small>
                </a>
              </li>
            </ul>
          </td>
        </tr>`
      );
    }
  },

  animation:
  {
    before: (task) =>
    {
      if(document.querySelector('.countdown') == null)
      {
        $('#task-container').append(
          `<div class='countdown'></div>`);
        $('#modal_completed > .modal-footer > .completed').attr('id', task.key);
      }

      audio.play();
      $('#pageStatus').html(`
        <h5>${task.title}</h5>
        <h4 id='timer'>25:00</h4>`);

      $('#dropdowns').html('');
    },

    after: () =>
    {
      audio.play();
      htmlrender.clear();
      $('#modal_completed > .modal-footer > .completed').removeAttr('id');
      $('#dropdowns').html(`
        <a class='dropdown-button ' href='#' data-activates='views'><i class="material-icons small">view_compact</i></a>
        <ul id='views' class='dropdown-content'>
          <li><a href="#!" id='cards'>cards<i class="material-icons small">view_agenda</i></a></li>
          <li><a href="#!" id='collapse'>list<i class="material-icons small">view_day</i></a></li>
          <li><a href="#!" id='table'>table<i class="material-icons small">view_list</i></a></li>
        </ul>
        <a class='dropdown-button ' href='#' data-activates='status'><i class="material-icons small">filter_list</i></a>
        <ul id='status' class='dropdown-content'>
          <li><a href="#!" id='completed'>main<i class="material-icons small">access_time</i></a></li>
          <li><a href="#!" id='archived'>archived<i class="material-icons small">archive</i></a></li>
        </ul>`);
    }
  },

  emptyList: () =>
  {
    $('#task-container').html(
      ` <div class='emptyList center-align'>
            <a href='#' class='center-align'><i class='material-icons large'>mood</i></a><br>
            <h5>Start typing to add your tasks!</h5>
        </div>`);
  },

  clear: () =>
  {
    $('.tooltipped').tooltip('remove');
    $('#pageStatus').html('');
    $('#timer').html('');
    $('#task-container').html('');
  },
}

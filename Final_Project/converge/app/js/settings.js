const settings =
{
    display: () =>
    {
      storage.has('display', (error, hasKey) =>
      {
        if (hasKey)
        {
          storage.get('display', (error, data) =>
          {
              htmlrender.type = data.display
          });
        }
        else
        {
          storage.set('display', { display: 'cards' }, (error) =>
          {
            if(error) throw error;
          });
        }
      });
    },

    changeDisplay: (pref) =>
    {
      if(pref)
      {
        storage.set('display', { display: pref }, (error) =>
        {
          if(error) throw error;
        });
        htmlrender.type = pref;
        tasks.display();
      }
    }
}

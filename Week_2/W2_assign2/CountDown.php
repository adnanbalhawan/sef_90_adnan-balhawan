#!/usr/bin/php
<?php
#SE Factory: Week 2 - Assignment 2 - Exercise 1 (CountDown)
#Author: Adnan Balhawan
#Date created: Wed 4th July, 2017

require_once 'GameGenerator.php';
require_once 'GameSolver.php';
//require_once 'GameOutput.php';

echo "How many games would you like me to play today?\n";
$NumberOfGames = readline('');

$inputValid = FALSE;

while(!is_numeric($NumberOfGames))
{
  echo "\nERROR, try again\nenter the number of games: ";
  $NumberOfGames = readline('');
}

$time_start = microtime(true); 

for($index = 0; $index < $NumberOfGames; $index++)
{
  $game = new GameGenerator();
  $SelectedCards = $game->SelectedCards();
  $targetNumber = $game->targetnumber();

  echo "Selected Cards: \n { ";
  foreach ($SelectedCards as $card)
  {
    echo $card." ";
  }
  echo "}\n";
  echo "Target number: ".$targetNumber."\n\n";
  
  $solve = new GameSolver;
  $solution = $solve->solve($SelectedCards, $targetNumber);
}

$time_end = microtime(true);

$execution_time = round(($time_end - $time_start),5);
echo "\nTotal Execution Time: ".$execution_time." seconds\n";
<?php 

class OperatorCmbinations {
		
		private $operators = array('*', '+', '-', '/');

		function Generate() {

				$operators     =  $this->operators;
				$possibilities =  array();


			 for ($i=0; $i < 4; $i++) {
						switch ($operators[$i]) {
								case '+':
										$order =  array('+', '-', '*', '/');
										break;
								case '-':
										$order =  array('-', '*', '/', '+');
										break;
								case '*':
										$order =  array('*', '/', '+', '-');
										break;
								case '/':
										$order =  array('/', '+', '-', '*');
										break;
						}

					 $fullOps =  array($order[0], $order[0], $order[0], $order[0], $order[0]);
						$possibilities[] =  $fullOps;

					 for ($j=0; $j < 5; $j++) {
								for ($k=0; $k < 4; $k++) {
										$fullOps[$j] =  $order[$k];
										$possibilities[] =  $fullOps;
								}
								$possibilities[] =  $fullOps;
						}
				}
				
				foreach($possibilities as $pos)
				{
						$pos = join(" ", $pos);
						
				}

				
				return $possibilities;

				
		}
}
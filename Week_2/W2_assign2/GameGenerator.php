<?php
#Author: Adnan Balhawan

class GameGenerator 
{
	private $TheBigNumbers = array(25, 50, 75, 100);
	private $TheSmallNumbers = array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
	private $CardsAllowed = 6;

	function SelectedCards()
	{
		$SelectedCards = array();

		shuffle($this->TheBigNumbers);
		shuffle($this->TheSmallNumbers);
		
		$CardsFromBigNumbers = rand(1,4);
		$CardsFromSmallNumbers = $this->CardsAllowed-$CardsFromBigNumbers;

		$BigRandomCardSelector = array_rand($this->TheBigNumbers, $CardsFromBigNumbers);
		$SmallRandomCardSelector = array_rand($this->TheSmallNumbers, $CardsFromSmallNumbers);

		if($CardsFromBigNumbers == 1)
		{
			$BigRandomCardSelector = array($BigRandomCardSelector);
		}

		$index = 0;
		$BigCardsSelectionIndex = 0;
		$SmallCardsSelectionIndex = 0;

		while ($index < $this->CardsAllowed)
		{
			while ($BigCardsSelectionIndex < $CardsFromBigNumbers)
			{
				$miniIndex = $BigRandomCardSelector[$BigCardsSelectionIndex];
				$SelectedCards[$index] = 
				$this->TheBigNumbers[$miniIndex];
				
				$BigCardsSelectionIndex++;
				$index ++;
			}
			while ($SmallCardsSelectionIndex < $CardsFromSmallNumbers) 
			{
				$miniIndex = $SmallRandomCardSelector[$SmallCardsSelectionIndex];
				$SelectedCards[$index] = 
				$this->TheSmallNumbers[$miniIndex];
				
				$SmallCardsSelectionIndex++;
				$index++;
			}
		}

		return $SelectedCards;
	}

	function TargetNumber()
	{
		$targetNumber = rand(101, 999);
		return $targetNumber;
	}
}
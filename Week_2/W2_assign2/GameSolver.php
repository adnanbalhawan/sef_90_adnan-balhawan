<?php

require_once 'OperatorCombinations.php';

class GameSolver
{

  function permute($combination,$i,$n, $InTarget) 
  {
    $target = $InTarget;
  
    $printLine = FALSE;

    for ($j = $i; $j < $n; $j++) 
    {
      $this->swap($combination,$i,$j);
      $this->permute($combination, $i+1, $n, $target);
      $this->swap($combination,$i,$j);

      $result = $this->execute($combination);

      if(empty($result))
      {
        break;
      }

      else
      {
        foreach ($combination as $line) 
        {
          if($result == $target)
          {
            echo $line." ";
            $printLine = TRUE;
          }
        }
        if($printLine == TRUE)
        {
          return $result;
        }
      }
    }
  }

  function swap (&$combination,$i,$j) 
  {
    $temp = $combination[$i];
    $combination[$i] = $combination[$j];
    $combination[$j] = $temp;
  }

  function is_operator($char) 
  {
  static $operators = array('+', '-', '/', '*', '%');
  return in_array($char, $operators);
  }   

  function solve ($numbers, $target)
  {
    $TheTarget = $target;

    $operators = array('+','-','/','*');    

    $OpComObj = new OperatorCmbinations();
    $OpCom = $OpComObj->Generate();
    shuffle($OpCom);

    $printLine = FALSE;
    foreach($OpCom as $comb)
    {
      $tempArray = array_merge($numbers, $comb);

      $result = $this->execute($tempArray);

      settype($result, "array");

      if(max($result) == $TheTarget)
      {
        $printLine = TRUE;
        foreach($tempArray as $elem)
        {
          echo $elem." ";
        }
        echo max($result)." FOUND IT !!!!";
        break;
      }
      

    }

    if($printLine == FALSE)
    {

      foreach ($OpCom as $comb) 
      {
        if
        (
          $comb[0] == $comb[1] and 
          $comb[1] == $comb[2] &&
          $comb[2] == $comb[3] &&
          $comb[3] == $comb[4] 
        )
        {
          continue;
        }
        else
        {
          $tempArray = array_merge($numbers, $comb);
          $result = $this->permute($tempArray, 0, 11, $target); 
          settype($result, "array");
          if(max($result) == $target)
          {
            $printLine = TRUE;
            print_r($tempArray);
            echo max($result)." FOUND IT !!!!";

            break;
          } 
        }
      }
    }
    return $stack->top();
  }

  function execute($rpnexp) 
  {
    $stack = array();

    foreach($rpnexp as $item) 
    {
      if(is_numeric($item))
      {
        $stack[] =  $item;
      }
      else if ($this->is_operator($item)) 
      {
        if ($item == '+') 
        {
          $j = array_pop($stack);
          $i = array_pop($stack);
          $stack[] =  $i + $j;
        }
        if ($item == '-') 
        {
          $j = array_pop($stack);
          $i = array_pop($stack);
          if($i > $j)
          {
          $stack[] =  $i - $j;
          }
        }
        if ($item == '*') 
        {
          $j = array_pop($stack);
          $i = array_pop($stack);
          if($i != 1 and $j != 1)
          {
            $stack[] =  $i * $j; 
          }
            }
        if ($item == '/') 
        {
          $j = array_pop($stack);
          $i = array_pop($stack);
          if($j != 0 and $i != 0 and $j % $i == 0 )
          {
            $stack[] =  (int) ( $i / $j);
          }
        }
      }
    } 

    settype($result, "array");

    return $stack;
  }
}


